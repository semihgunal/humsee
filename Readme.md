# Humsee
- Humsee bir proje yönetim ve takip otomasyonudur.

# Problem
- Özellikle birden fazla veya büyük projeler üzerinde çalışan şirketlerde projelerin ve çalışanların takibi zorlaşır. Hangi proje ne durumda, hangi projede hangi adımdayız, proje yaklaşık olarak ne zaman biter, proje çalışanları projeye ne kadar katkı veriyor, proje çalışanları projede verilen görevleri zamanında bitiriyor mu, proje görevlerinin dağılımı düzgün mü gibi aklımıza gelebilecek birçok soruyu cevaplamak oldukça zordur. Sadece bir sorunun cevabı bile birçok şeyi değiştirecektir. Örneğin bir çalışan projeye diğer çalışanlardan daha fazla katkı vermesi ve diğer çalışanlarla aynı değeri görmesi halinde bu çalışanın hevesinin kırılmasına neden olacaktır. Daha sonraki projelerde diğer çalışanlar gibi daha az katkı versem de yeter düşüncesine kapılacaktır. Buda proje süresinin uzaması ve çalışanlardan daha az verim alınmasına neden olur. Bu sorulara cevap verebilecek bir proje yönetim ve takip otomasyonu olması şirketin verimliliğini, şirket çalışanlarının hak ettikleri kadar değer görmesini sağlamanın yanında projelerin yönetimini ve takibini oldukça kolaylaştıracaktır.

# Proje Üyeleri
- 330213 Semih GÜNAL
- 330155 Yunus Emre BERBER

# Proje Araçları
- Visual Studio
- Metro Modern UI
- SQL Server Management Studio

# Gereksinim Analizi

# Yazılımsal (Fonksiyonel) Gereksinimler
+ Üye Giriş Sayfası:
- Humsee programına kayıtlı üyelerin kullanıcı adı ve şifre ile giriş yapmasını sağlayan sayfa.
- Aynı zamanda üyelikleri yoksa üyelik açma sayfasına geçmek için üye ol butonu yer alacak.
+ Üyelik Açma Sayfası:
- Kullanıcıların programı kullanabilmesi amacıyla temel bilgiler ve güvenlik için parola isteyerek kolaylıkla üyelik açmalarını sağlayan sayfa.
+ Ana Sayfa:
- Bu sayfada yeni proje oluşturma ve daha önceden oluşturulan projeyi açmak için iki adet buton yer alacak.
- Yeni proje oluşturma butonuna tıklandığında proje oluşturma sayfasına geçilecek.
+ Proje Oluşturma Sayfası:
- Bu sayfada projenin adı, amacı, üyeleri ve projede kullanılacak dil, araçlar... gibi temel bilgiler girilecek ve yeni bir proje oluşturulabilecek.
- Proje oluşturulduktan sonra proje yönetim ve takip sayfasına geçilecek.
+ Proje Yönetim ve Takip Sayfası:
- Proje yönetimini ve takibini sağlayan iki ayrı kolon bulunacak.
- Proje takibi kolonunda proje akışı tarihsel olarak sıralı bir şekilde görüntülenecek.
- Proje yönetim kolonunda proje bilgilerini düzenleme, projeye yeni üyeler ekleme, proje adımlarını belirleme veya düzenleme, proje üyelerine görevler atama, proje üyelerinin istatistiklerini görüntüleme, projeyi silme gibi ana fonksiyonların olduğu butonlar yer alacak.
- Üyeler görevleri yerine getirdikleri zaman proje yöneticisi sisteme girip görevin bittiğini onaylayacak ve görevi ne kadar iyi yaptığını oylayabilecek.
- Bu fonksiyonların haricinde bildirimleri gösteren bir fonksiyon olacak. Örneğin proje üyelerinin verilen görevleri geciktirdiğine dair bilgilendirme tarzı bildirimler olacak.

# Donanımsal (Fonksiyonel Olmayan) Gereksinimler
- Windows işletim sistemi kurulu olan bir bilgisayar yeterli olacak.
﻿using MetroFramework;
using System.Windows.Forms;
using MetroFramework.Controls;

namespace Humsee
{
    class HomeUnit
    {
        public static void AddWorker(string fname, string lname, string position, string email, string phone)
        {
            // Test Variables
            bool Input_Null_Test = (fname != "" && lname != "" && position != "" && email != "" && phone != "");
            bool Input_Space_Test = (fname.IndexOf(" ") < 0 && lname.IndexOf(" ") < 0 && position.IndexOf(" ") < 0 && email.IndexOf(" ") < 0 && phone.IndexOf(" ") < 0);
            
            // [Code 002] Input Null Test
            if (Input_Null_Test)
            {
                // [Code 003] Input Space Test
                if (Input_Space_Test)
                {
                    DataBaseUnit.WorkerInsert(fname, lname, position, email, phone);
                    MetroMessageBox.Show(New_Worker.ActiveForm, "New worker has been successfully added", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    New_Worker.ActiveForm.Dispose();
                }
                else
                {
                    MetroMessageBox.Show(New_Worker.ActiveForm, "inputs can not contain space", "Error! [Code 003]", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MetroMessageBox.Show(New_Worker.ActiveForm, "inputs can not contain null value", "Error! [Code 002]", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        public static void RemoveWorker(string worker)
        {
            DataBaseUnit.WorkerRemove(worker);
            MetroMessageBox.Show(Home.ActiveForm, "Worker has been successfully removed", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void AddTask(string worker, string task, string tstatus, string end)
        {
            // [Code 002] Input Null Test
            if (task != "")
            {
                DataBaseUnit.TaskInsert(worker, task, tstatus, end);
                MetroMessageBox.Show(New_Task.ActiveForm, "New task has been successfully added", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                New_Task.ActiveForm.Dispose();
            }
            else
            {
                MetroMessageBox.Show(New_Task.ActiveForm, "inputs can not contain null value", "Error! [Code 002]", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        public static void RemoveTask(string task)
        {
            DataBaseUnit.TaskRemove(task);
            MetroMessageBox.Show(Home.ActiveForm, "task has been successfully removed", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void DoneTask(string task)
        {
            DataBaseUnit.TaskDone(task);
            MetroMessageBox.Show(Home.ActiveForm, "task has been successfully finished", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void SetProjectSettings (MetroTextBox description, MetroComboBox language, MetroTextBox link, MetroTextBox end)
        {
            // Project Table Read
            DataBaseUnit.ProjectRead();
            // Settings Project Info Load
            description.Text = DataBaseUnit.ProjectTable[1];
            language.Text = DataBaseUnit.ProjectTable[2];
            link.Text = DataBaseUnit.ProjectTable[3];
            end.Text = DataBaseUnit.ProjectTable[4];
        }

        public static void UpdateProject(string description, string language, string link)
        {
            // [Code 002] Input Null Test
            if (description != "" && link != "")
            {
                // [Code 003] Input Space Test
                if (link.IndexOf(" ") < 0)
                {
                    DataBaseUnit.ProjectUpdate(description, language, link);
                    MetroMessageBox.Show(Settings.ActiveForm, "project has been successfully updated", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Settings.ActiveForm.Dispose();
                }
                else
                {
                    MetroMessageBox.Show(Settings.ActiveForm, "inputs can not contain space", "Error! [Code 003]", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MetroMessageBox.Show(Settings.ActiveForm, "inputs can not contain null value", "Error! [Code 002]", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
    }
}

﻿using System;
using MetroFramework;
using MetroFramework.Forms;
using System.Windows.Forms;

namespace Humsee
{
    public partial class Project : MetroForm
    {
        public Project()
        {
            InitializeComponent();
        }

        private void Project_Activated(object sender, EventArgs e)
        {
            Project_Refresh();
        }

        private void Project_Refresh()
        {
            // Data Base Fill
            this.projectsTableAdapter.Fill(this.humseeDataSetProjects.Projects, ProjectUnit.UserName);
        }

        private void Btn_Open_Click(object sender, EventArgs e)
        {
            // [Code 002] Input Null Test
            if (cb_select_project.Text != "")
            {
                // Project Settings Read
                DataBaseUnit.ProjectRead();
                // Project Name Read
                DataBaseUnit.ProjectTable[0] = cb_select_project.Text;
                // Open Home Form
                Home Form = new Home();
                Form.ShowDialog();
            }
            else
            {
                MetroMessageBox.Show(Register.ActiveForm, "Please select project", "Error! [Code 002]", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void Btn_New_Project_Click(object sender, EventArgs e)
        {
            New_Project Form = new New_Project();
            Form.ShowDialog();
        }

        private void Btn_Logout_Click(object sender, EventArgs e)
        {
            // Logout
            Application.Restart();
        }
    }
}

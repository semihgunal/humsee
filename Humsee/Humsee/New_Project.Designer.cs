﻿namespace Humsee
{
    partial class New_Project
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.lbl_name = new MetroFramework.Controls.MetroLabel();
            this.lbl_description = new MetroFramework.Controls.MetroLabel();
            this.lbl_language = new MetroFramework.Controls.MetroLabel();
            this.lbl_link = new MetroFramework.Controls.MetroLabel();
            this.lbl_end_date = new MetroFramework.Controls.MetroLabel();
            this.tb_name = new MetroFramework.Controls.MetroTextBox();
            this.tb_description = new MetroFramework.Controls.MetroTextBox();
            this.cb_language = new MetroFramework.Controls.MetroComboBox();
            this.tb_link = new MetroFramework.Controls.MetroTextBox();
            this.dt_end = new MetroFramework.Controls.MetroDateTime();
            this.btn_cancel = new MetroFramework.Controls.MetroButton();
            this.lbl_space02 = new MetroFramework.Controls.MetroLabel();
            this.btn_create = new MetroFramework.Controls.MetroButton();
            this.lbl_space01 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // lbl_name
            // 
            this.lbl_name.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_name.Enabled = false;
            this.lbl_name.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_name.Location = new System.Drawing.Point(20, 60);
            this.lbl_name.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_name.Name = "lbl_name";
            this.lbl_name.Size = new System.Drawing.Size(260, 25);
            this.lbl_name.TabIndex = 0;
            this.lbl_name.Text = "Name";
            this.lbl_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_name.UseMnemonic = false;
            // 
            // lbl_description
            // 
            this.lbl_description.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_description.Enabled = false;
            this.lbl_description.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_description.Location = new System.Drawing.Point(20, 115);
            this.lbl_description.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_description.Name = "lbl_description";
            this.lbl_description.Size = new System.Drawing.Size(260, 25);
            this.lbl_description.TabIndex = 2;
            this.lbl_description.Text = "Description";
            this.lbl_description.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_description.UseMnemonic = false;
            // 
            // lbl_language
            // 
            this.lbl_language.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_language.Enabled = false;
            this.lbl_language.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_language.Location = new System.Drawing.Point(20, 172);
            this.lbl_language.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_language.Name = "lbl_language";
            this.lbl_language.Size = new System.Drawing.Size(260, 25);
            this.lbl_language.TabIndex = 4;
            this.lbl_language.Text = "Language";
            this.lbl_language.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_language.UseMnemonic = false;
            // 
            // lbl_link
            // 
            this.lbl_link.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_link.Enabled = false;
            this.lbl_link.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_link.Location = new System.Drawing.Point(20, 226);
            this.lbl_link.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_link.Name = "lbl_link";
            this.lbl_link.Size = new System.Drawing.Size(260, 25);
            this.lbl_link.TabIndex = 6;
            this.lbl_link.Text = "Link";
            this.lbl_link.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_link.UseMnemonic = false;
            // 
            // lbl_end_date
            // 
            this.lbl_end_date.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_end_date.Enabled = false;
            this.lbl_end_date.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_end_date.Location = new System.Drawing.Point(20, 281);
            this.lbl_end_date.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_end_date.Name = "lbl_end_date";
            this.lbl_end_date.Size = new System.Drawing.Size(260, 25);
            this.lbl_end_date.TabIndex = 8;
            this.lbl_end_date.Text = "End Date";
            this.lbl_end_date.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_end_date.UseMnemonic = false;
            // 
            // tb_name
            // 
            this.tb_name.CustomButton.Image = null;
            this.tb_name.CustomButton.Location = new System.Drawing.Point(232, 2);
            this.tb_name.CustomButton.Name = "";
            this.tb_name.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tb_name.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_name.CustomButton.TabIndex = 1;
            this.tb_name.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_name.CustomButton.UseSelectable = true;
            this.tb_name.CustomButton.Visible = false;
            this.tb_name.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_name.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_name.Lines = new string[0];
            this.tb_name.Location = new System.Drawing.Point(20, 85);
            this.tb_name.Margin = new System.Windows.Forms.Padding(0);
            this.tb_name.MaxLength = 30;
            this.tb_name.Name = "tb_name";
            this.tb_name.PasswordChar = '\0';
            this.tb_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_name.SelectedText = "";
            this.tb_name.SelectionLength = 0;
            this.tb_name.SelectionStart = 0;
            this.tb_name.ShortcutsEnabled = true;
            this.tb_name.Size = new System.Drawing.Size(260, 30);
            this.tb_name.TabIndex = 1;
            this.tb_name.UseSelectable = true;
            this.tb_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_name.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            // 
            // tb_description
            // 
            this.tb_description.CustomButton.Image = null;
            this.tb_description.CustomButton.Location = new System.Drawing.Point(230, 2);
            this.tb_description.CustomButton.Name = "";
            this.tb_description.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.tb_description.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_description.CustomButton.TabIndex = 1;
            this.tb_description.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_description.CustomButton.UseSelectable = true;
            this.tb_description.CustomButton.Visible = false;
            this.tb_description.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_description.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_description.Lines = new string[0];
            this.tb_description.Location = new System.Drawing.Point(20, 140);
            this.tb_description.Margin = new System.Windows.Forms.Padding(0);
            this.tb_description.MaxLength = 100;
            this.tb_description.Multiline = true;
            this.tb_description.Name = "tb_description";
            this.tb_description.PasswordChar = '\0';
            this.tb_description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_description.SelectedText = "";
            this.tb_description.SelectionLength = 0;
            this.tb_description.SelectionStart = 0;
            this.tb_description.ShortcutsEnabled = true;
            this.tb_description.Size = new System.Drawing.Size(260, 32);
            this.tb_description.TabIndex = 3;
            this.tb_description.UseSelectable = true;
            this.tb_description.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_description.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            // 
            // cb_language
            // 
            this.cb_language.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb_language.Dock = System.Windows.Forms.DockStyle.Top;
            this.cb_language.DropDownHeight = 125;
            this.cb_language.FormattingEnabled = true;
            this.cb_language.IntegralHeight = false;
            this.cb_language.ItemHeight = 23;
            this.cb_language.Items.AddRange(new object[] {
            "C",
            "C#",
            "C++",
            "Go",
            "HTML/CSS",
            "Java",
            "JavaScript",
            "Objective-C",
            "Perl",
            "PHP",
            "Python",
            "Ruby",
            "Swift",
            "Other"});
            this.cb_language.Location = new System.Drawing.Point(20, 197);
            this.cb_language.Margin = new System.Windows.Forms.Padding(0);
            this.cb_language.MaxDropDownItems = 5;
            this.cb_language.Name = "cb_language";
            this.cb_language.Size = new System.Drawing.Size(260, 29);
            this.cb_language.TabIndex = 5;
            this.cb_language.UseSelectable = true;
            // 
            // tb_link
            // 
            this.tb_link.CustomButton.Image = null;
            this.tb_link.CustomButton.Location = new System.Drawing.Point(232, 2);
            this.tb_link.CustomButton.Name = "";
            this.tb_link.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tb_link.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_link.CustomButton.TabIndex = 1;
            this.tb_link.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_link.CustomButton.UseSelectable = true;
            this.tb_link.CustomButton.Visible = false;
            this.tb_link.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_link.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_link.Lines = new string[0];
            this.tb_link.Location = new System.Drawing.Point(20, 251);
            this.tb_link.Margin = new System.Windows.Forms.Padding(0);
            this.tb_link.MaxLength = 50;
            this.tb_link.Name = "tb_link";
            this.tb_link.PasswordChar = '\0';
            this.tb_link.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_link.SelectedText = "";
            this.tb_link.SelectionLength = 0;
            this.tb_link.SelectionStart = 0;
            this.tb_link.ShortcutsEnabled = true;
            this.tb_link.Size = new System.Drawing.Size(260, 30);
            this.tb_link.TabIndex = 7;
            this.tb_link.UseSelectable = true;
            this.tb_link.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_link.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            // 
            // dt_end
            // 
            this.dt_end.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dt_end.CustomFormat = "dd.MM.yyyy";
            this.dt_end.Dock = System.Windows.Forms.DockStyle.Top;
            this.dt_end.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dt_end.Location = new System.Drawing.Point(20, 306);
            this.dt_end.Margin = new System.Windows.Forms.Padding(0);
            this.dt_end.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dt_end.MinimumSize = new System.Drawing.Size(0, 29);
            this.dt_end.Name = "dt_end";
            this.dt_end.Size = new System.Drawing.Size(260, 29);
            this.dt_end.TabIndex = 9;
            this.dt_end.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackColor = System.Drawing.Color.Crimson;
            this.btn_cancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_cancel.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_cancel.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_cancel.ForeColor = System.Drawing.Color.White;
            this.btn_cancel.Location = new System.Drawing.Point(20, 385);
            this.btn_cancel.Margin = new System.Windows.Forms.Padding(0);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(260, 35);
            this.btn_cancel.TabIndex = 13;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseCustomBackColor = true;
            this.btn_cancel.UseCustomForeColor = true;
            this.btn_cancel.UseMnemonic = false;
            this.btn_cancel.UseSelectable = true;
            this.btn_cancel.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // lbl_space02
            // 
            this.lbl_space02.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_space02.Enabled = false;
            this.lbl_space02.Location = new System.Drawing.Point(20, 380);
            this.lbl_space02.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_space02.Name = "lbl_space02";
            this.lbl_space02.Size = new System.Drawing.Size(260, 5);
            this.lbl_space02.TabIndex = 12;
            this.lbl_space02.UseMnemonic = false;
            // 
            // btn_create
            // 
            this.btn_create.BackColor = System.Drawing.Color.Orange;
            this.btn_create.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_create.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_create.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_create.ForeColor = System.Drawing.Color.White;
            this.btn_create.Location = new System.Drawing.Point(20, 345);
            this.btn_create.Margin = new System.Windows.Forms.Padding(0);
            this.btn_create.Name = "btn_create";
            this.btn_create.Size = new System.Drawing.Size(260, 35);
            this.btn_create.TabIndex = 11;
            this.btn_create.Text = "Create";
            this.btn_create.UseCustomBackColor = true;
            this.btn_create.UseCustomForeColor = true;
            this.btn_create.UseMnemonic = false;
            this.btn_create.UseSelectable = true;
            this.btn_create.Click += new System.EventHandler(this.Btn_Create_Click);
            // 
            // lbl_space01
            // 
            this.lbl_space01.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_space01.Enabled = false;
            this.lbl_space01.Location = new System.Drawing.Point(20, 335);
            this.lbl_space01.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_space01.Name = "lbl_space01";
            this.lbl_space01.Size = new System.Drawing.Size(260, 10);
            this.lbl_space01.TabIndex = 10;
            this.lbl_space01.UseMnemonic = false;
            // 
            // New_Project
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(300, 440);
            this.ControlBox = false;
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.lbl_space02);
            this.Controls.Add(this.btn_create);
            this.Controls.Add(this.lbl_space01);
            this.Controls.Add(this.dt_end);
            this.Controls.Add(this.lbl_end_date);
            this.Controls.Add(this.tb_link);
            this.Controls.Add(this.lbl_link);
            this.Controls.Add(this.cb_language);
            this.Controls.Add(this.lbl_language);
            this.Controls.Add(this.tb_description);
            this.Controls.Add(this.lbl_description);
            this.Controls.Add(this.tb_name);
            this.Controls.Add(this.lbl_name);
            this.Font = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Movable = false;
            this.Name = "New_Project";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Style = MetroFramework.MetroColorStyle.Orange;
            this.Text = "New Project";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.New_Project_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox tb_name;
        private MetroFramework.Controls.MetroTextBox tb_description;
        private MetroFramework.Controls.MetroComboBox cb_language;
        private MetroFramework.Controls.MetroTextBox tb_link;
        private MetroFramework.Controls.MetroLabel lbl_name;
        private MetroFramework.Controls.MetroLabel lbl_description;
        private MetroFramework.Controls.MetroLabel lbl_language;
        private MetroFramework.Controls.MetroLabel lbl_link;
        private MetroFramework.Controls.MetroLabel lbl_end_date;
        private MetroFramework.Controls.MetroDateTime dt_end;
        private MetroFramework.Controls.MetroButton btn_cancel;
        private MetroFramework.Controls.MetroLabel lbl_space02;
        private MetroFramework.Controls.MetroButton btn_create;
        private MetroFramework.Controls.MetroLabel lbl_space01;
    }
}

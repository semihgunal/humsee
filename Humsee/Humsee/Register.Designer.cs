﻿namespace Humsee
{
    partial class Register
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            MetroFramework.Controls.MetroLabel lbl_user_name;
            MetroFramework.Controls.MetroLabel lbl_first_name;
            MetroFramework.Controls.MetroLabel lbl_last_name;
            MetroFramework.Controls.MetroLabel lbl_password;
            MetroFramework.Controls.MetroLabel lbl_confirm_password;
            MetroFramework.Controls.MetroLabel lbl_space01;
            MetroFramework.Controls.MetroLabel lbl_space02;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Register));
            this.tb_username = new MetroFramework.Controls.MetroTextBox();
            this.tb_first_name = new MetroFramework.Controls.MetroTextBox();
            this.tb_last_name = new MetroFramework.Controls.MetroTextBox();
            this.tb_password = new MetroFramework.Controls.MetroTextBox();
            this.tb_confirm_password = new MetroFramework.Controls.MetroTextBox();
            this.btn_register = new MetroFramework.Controls.MetroButton();
            this.btn_cancel = new MetroFramework.Controls.MetroButton();
            lbl_user_name = new MetroFramework.Controls.MetroLabel();
            lbl_first_name = new MetroFramework.Controls.MetroLabel();
            lbl_last_name = new MetroFramework.Controls.MetroLabel();
            lbl_password = new MetroFramework.Controls.MetroLabel();
            lbl_confirm_password = new MetroFramework.Controls.MetroLabel();
            lbl_space01 = new MetroFramework.Controls.MetroLabel();
            lbl_space02 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // lbl_user_name
            // 
            lbl_user_name.Dock = System.Windows.Forms.DockStyle.Top;
            lbl_user_name.Enabled = false;
            lbl_user_name.FontSize = MetroFramework.MetroLabelSize.Tall;
            lbl_user_name.Location = new System.Drawing.Point(20, 60);
            lbl_user_name.Margin = new System.Windows.Forms.Padding(0);
            lbl_user_name.Name = "lbl_user_name";
            lbl_user_name.Size = new System.Drawing.Size(260, 25);
            lbl_user_name.TabIndex = 0;
            lbl_user_name.Text = "User Name";
            lbl_user_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            lbl_user_name.UseMnemonic = false;
            // 
            // lbl_first_name
            // 
            lbl_first_name.Dock = System.Windows.Forms.DockStyle.Top;
            lbl_first_name.Enabled = false;
            lbl_first_name.FontSize = MetroFramework.MetroLabelSize.Tall;
            lbl_first_name.Location = new System.Drawing.Point(20, 115);
            lbl_first_name.Margin = new System.Windows.Forms.Padding(0);
            lbl_first_name.Name = "lbl_first_name";
            lbl_first_name.Size = new System.Drawing.Size(260, 25);
            lbl_first_name.TabIndex = 2;
            lbl_first_name.Text = "First Name";
            lbl_first_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            lbl_first_name.UseMnemonic = false;
            // 
            // lbl_last_name
            // 
            lbl_last_name.Dock = System.Windows.Forms.DockStyle.Top;
            lbl_last_name.Enabled = false;
            lbl_last_name.FontSize = MetroFramework.MetroLabelSize.Tall;
            lbl_last_name.Location = new System.Drawing.Point(20, 170);
            lbl_last_name.Margin = new System.Windows.Forms.Padding(0);
            lbl_last_name.Name = "lbl_last_name";
            lbl_last_name.Size = new System.Drawing.Size(260, 25);
            lbl_last_name.TabIndex = 4;
            lbl_last_name.Text = "Last Name";
            lbl_last_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            lbl_last_name.UseMnemonic = false;
            // 
            // lbl_password
            // 
            lbl_password.Dock = System.Windows.Forms.DockStyle.Top;
            lbl_password.Enabled = false;
            lbl_password.FontSize = MetroFramework.MetroLabelSize.Tall;
            lbl_password.Location = new System.Drawing.Point(20, 225);
            lbl_password.Margin = new System.Windows.Forms.Padding(0);
            lbl_password.Name = "lbl_password";
            lbl_password.Size = new System.Drawing.Size(260, 25);
            lbl_password.TabIndex = 6;
            lbl_password.Text = "Password";
            lbl_password.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            lbl_password.UseMnemonic = false;
            // 
            // lbl_confirm_password
            // 
            lbl_confirm_password.Dock = System.Windows.Forms.DockStyle.Top;
            lbl_confirm_password.Enabled = false;
            lbl_confirm_password.FontSize = MetroFramework.MetroLabelSize.Tall;
            lbl_confirm_password.Location = new System.Drawing.Point(20, 280);
            lbl_confirm_password.Margin = new System.Windows.Forms.Padding(0);
            lbl_confirm_password.Name = "lbl_confirm_password";
            lbl_confirm_password.Size = new System.Drawing.Size(260, 25);
            lbl_confirm_password.TabIndex = 8;
            lbl_confirm_password.Text = "Confirm Password";
            lbl_confirm_password.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            lbl_confirm_password.UseMnemonic = false;
            // 
            // lbl_space01
            // 
            lbl_space01.Dock = System.Windows.Forms.DockStyle.Top;
            lbl_space01.Enabled = false;
            lbl_space01.Location = new System.Drawing.Point(20, 335);
            lbl_space01.Margin = new System.Windows.Forms.Padding(0);
            lbl_space01.Name = "lbl_space01";
            lbl_space01.Size = new System.Drawing.Size(260, 10);
            lbl_space01.TabIndex = 10;
            lbl_space01.UseMnemonic = false;
            // 
            // lbl_space02
            // 
            lbl_space02.Dock = System.Windows.Forms.DockStyle.Top;
            lbl_space02.Enabled = false;
            lbl_space02.Location = new System.Drawing.Point(20, 380);
            lbl_space02.Margin = new System.Windows.Forms.Padding(0);
            lbl_space02.Name = "lbl_space02";
            lbl_space02.Size = new System.Drawing.Size(260, 5);
            lbl_space02.TabIndex = 12;
            lbl_space02.UseMnemonic = false;
            // 
            // tb_username
            //  
            this.tb_username.CustomButton.Image = null;
            this.tb_username.CustomButton.Location = new System.Drawing.Point(232, 2);
            this.tb_username.CustomButton.Name = "";
            this.tb_username.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tb_username.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_username.CustomButton.TabIndex = 1;
            this.tb_username.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_username.CustomButton.UseSelectable = true;
            this.tb_username.CustomButton.Visible = false;
            this.tb_username.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_username.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_username.Lines = new string[0];
            this.tb_username.Location = new System.Drawing.Point(20, 85);
            this.tb_username.Margin = new System.Windows.Forms.Padding(0);
            this.tb_username.MaxLength = 15;
            this.tb_username.Name = "tb_username";
            this.tb_username.PasswordChar = '\0';
            this.tb_username.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_username.SelectedText = "";
            this.tb_username.SelectionLength = 0;
            this.tb_username.SelectionStart = 0;
            this.tb_username.ShortcutsEnabled = true;
            this.tb_username.Size = new System.Drawing.Size(260, 30);
            this.tb_username.TabIndex = 1;
            this.tb_username.UseSelectable = true;
            this.tb_username.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_username.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            // 
            // tb_first_name
            // 
            this.tb_first_name.CustomButton.Image = null;
            this.tb_first_name.CustomButton.Location = new System.Drawing.Point(232, 2);
            this.tb_first_name.CustomButton.Name = "";
            this.tb_first_name.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tb_first_name.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_first_name.CustomButton.TabIndex = 1;
            this.tb_first_name.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_first_name.CustomButton.UseSelectable = true;
            this.tb_first_name.CustomButton.Visible = false;
            this.tb_first_name.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_first_name.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_first_name.Lines = new string[0];
            this.tb_first_name.Location = new System.Drawing.Point(20, 140);
            this.tb_first_name.Margin = new System.Windows.Forms.Padding(0);
            this.tb_first_name.MaxLength = 15;
            this.tb_first_name.Name = "tb_first_name";
            this.tb_first_name.PasswordChar = '\0';
            this.tb_first_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_first_name.SelectedText = "";
            this.tb_first_name.SelectionLength = 0;
            this.tb_first_name.SelectionStart = 0;
            this.tb_first_name.ShortcutsEnabled = true;
            this.tb_first_name.Size = new System.Drawing.Size(260, 30);
            this.tb_first_name.TabIndex = 3;
            this.tb_first_name.UseSelectable = true;
            this.tb_first_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_first_name.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            // 
            // tb_last_name
            // 
            this.tb_last_name.CustomButton.Image = null;
            this.tb_last_name.CustomButton.Location = new System.Drawing.Point(232, 2);
            this.tb_last_name.CustomButton.Name = "";
            this.tb_last_name.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tb_last_name.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_last_name.CustomButton.TabIndex = 1;
            this.tb_last_name.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_last_name.CustomButton.UseSelectable = true;
            this.tb_last_name.CustomButton.Visible = false;
            this.tb_last_name.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_last_name.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_last_name.Lines = new string[0];
            this.tb_last_name.Location = new System.Drawing.Point(20, 195);
            this.tb_last_name.Margin = new System.Windows.Forms.Padding(0);
            this.tb_last_name.MaxLength = 15;
            this.tb_last_name.Name = "tb_last_name";
            this.tb_last_name.PasswordChar = '\0';
            this.tb_last_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_last_name.SelectedText = "";
            this.tb_last_name.SelectionLength = 0;
            this.tb_last_name.SelectionStart = 0;
            this.tb_last_name.ShortcutsEnabled = true;
            this.tb_last_name.Size = new System.Drawing.Size(260, 30);
            this.tb_last_name.TabIndex = 5;
            this.tb_last_name.UseSelectable = true;
            this.tb_last_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_last_name.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            // 
            // tb_password
            // 
            this.tb_password.CustomButton.Image = null;
            this.tb_password.CustomButton.Location = new System.Drawing.Point(232, 2);
            this.tb_password.CustomButton.Name = "";
            this.tb_password.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tb_password.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_password.CustomButton.TabIndex = 1;
            this.tb_password.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_password.CustomButton.UseSelectable = true;
            this.tb_password.CustomButton.Visible = false;
            this.tb_password.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_password.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_password.Lines = new string[0];
            this.tb_password.Location = new System.Drawing.Point(20, 250);
            this.tb_password.Margin = new System.Windows.Forms.Padding(0);
            this.tb_password.MaxLength = 15;
            this.tb_password.Name = "tb_password";
            this.tb_password.PasswordChar = '●';
            this.tb_password.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_password.SelectedText = "";
            this.tb_password.SelectionLength = 0;
            this.tb_password.SelectionStart = 0;
            this.tb_password.ShortcutsEnabled = true;
            this.tb_password.Size = new System.Drawing.Size(260, 30);
            this.tb_password.TabIndex = 7;
            this.tb_password.UseSelectable = true;
            this.tb_password.UseSystemPasswordChar = true;
            this.tb_password.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_password.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            // 
            // tb_confirm_password
            // 
            this.tb_confirm_password.CustomButton.Image = null;
            this.tb_confirm_password.CustomButton.Location = new System.Drawing.Point(232, 2);
            this.tb_confirm_password.CustomButton.Name = "";
            this.tb_confirm_password.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tb_confirm_password.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_confirm_password.CustomButton.TabIndex = 1;
            this.tb_confirm_password.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_confirm_password.CustomButton.UseSelectable = true;
            this.tb_confirm_password.CustomButton.Visible = false;
            this.tb_confirm_password.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_confirm_password.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_confirm_password.Lines = new string[0];
            this.tb_confirm_password.Location = new System.Drawing.Point(20, 305);
            this.tb_confirm_password.Margin = new System.Windows.Forms.Padding(0);
            this.tb_confirm_password.MaxLength = 15;
            this.tb_confirm_password.Name = "tb_confirm_password";
            this.tb_confirm_password.PasswordChar = '●';
            this.tb_confirm_password.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_confirm_password.SelectedText = "";
            this.tb_confirm_password.SelectionLength = 0;
            this.tb_confirm_password.SelectionStart = 0;
            this.tb_confirm_password.ShortcutsEnabled = true;
            this.tb_confirm_password.Size = new System.Drawing.Size(260, 30);
            this.tb_confirm_password.TabIndex = 9;
            this.tb_confirm_password.UseSelectable = true;
            this.tb_confirm_password.UseSystemPasswordChar = true;
            this.tb_confirm_password.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_confirm_password.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            // 
            // btn_register
            // 
            this.btn_register.BackColor = System.Drawing.Color.RoyalBlue;
            this.btn_register.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_register.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_register.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_register.ForeColor = System.Drawing.Color.White;
            this.btn_register.Location = new System.Drawing.Point(20, 345);
            this.btn_register.Margin = new System.Windows.Forms.Padding(0);
            this.btn_register.Name = "btn_register";
            this.btn_register.Size = new System.Drawing.Size(260, 35);
            this.btn_register.TabIndex = 11;
            this.btn_register.Text = "Register";
            this.btn_register.UseCustomBackColor = true;
            this.btn_register.UseCustomForeColor = true;
            this.btn_register.UseMnemonic = false;
            this.btn_register.UseSelectable = true;
            this.btn_register.Click += new System.EventHandler(this.Btn_Register_Click);
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackColor = System.Drawing.Color.Crimson;
            this.btn_cancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_cancel.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_cancel.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_cancel.ForeColor = System.Drawing.Color.White;
            this.btn_cancel.Location = new System.Drawing.Point(20, 385);
            this.btn_cancel.Margin = new System.Windows.Forms.Padding(0);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(260, 35);
            this.btn_cancel.TabIndex = 13;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseCustomBackColor = true;
            this.btn_cancel.UseCustomForeColor = true;
            this.btn_cancel.UseMnemonic = false;
            this.btn_cancel.UseSelectable = true;
            this.btn_cancel.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // Register
            // 
            this.AcceptButton = this.btn_register;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton = this.btn_cancel;
            this.ClientSize = new System.Drawing.Size(300, 440);
            this.ControlBox = false;
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(lbl_space02);
            this.Controls.Add(this.btn_register);
            this.Controls.Add(lbl_space01);
            this.Controls.Add(this.tb_confirm_password);
            this.Controls.Add(lbl_confirm_password);
            this.Controls.Add(this.tb_password);
            this.Controls.Add(lbl_password);
            this.Controls.Add(this.tb_last_name);
            this.Controls.Add(lbl_last_name);
            this.Controls.Add(this.tb_first_name);
            this.Controls.Add(lbl_first_name);
            this.Controls.Add(this.tb_username);
            this.Controls.Add(lbl_user_name);
            this.Font = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Movable = false;
            this.Name = "Register";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Humsee Register";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox tb_username;
        private MetroFramework.Controls.MetroTextBox tb_first_name;
        private MetroFramework.Controls.MetroTextBox tb_last_name;
        private MetroFramework.Controls.MetroTextBox tb_password;
        private MetroFramework.Controls.MetroTextBox tb_confirm_password;
        private MetroFramework.Controls.MetroButton btn_register;
        private MetroFramework.Controls.MetroButton btn_cancel;
    }
}

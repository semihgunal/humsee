﻿namespace Humsee
{
    partial class Login
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.lbl_user_name = new MetroFramework.Controls.MetroLabel();
            this.lbl_password = new MetroFramework.Controls.MetroLabel();
            this.lbl_space01 = new MetroFramework.Controls.MetroLabel();
            this.lbl_space02 = new MetroFramework.Controls.MetroLabel();
            this.tb_user_name = new MetroFramework.Controls.MetroTextBox();
            this.tb_password = new MetroFramework.Controls.MetroTextBox();
            this.btn_login = new MetroFramework.Controls.MetroButton();
            this.btn_create_account = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // lbl_user_name
            // 
            this.lbl_user_name.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_user_name.Enabled = false;
            this.lbl_user_name.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_user_name.Location = new System.Drawing.Point(20, 60);
            this.lbl_user_name.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_user_name.Name = "lbl_user_name";
            this.lbl_user_name.Size = new System.Drawing.Size(260, 25);
            this.lbl_user_name.TabIndex = 0;
            this.lbl_user_name.Text = "User Name";
            this.lbl_user_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_user_name.UseMnemonic = false;
            // 
            // lbl_password
            // 
            this.lbl_password.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_password.Enabled = false;
            this.lbl_password.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_password.Location = new System.Drawing.Point(20, 115);
            this.lbl_password.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_password.Name = "lbl_password";
            this.lbl_password.Size = new System.Drawing.Size(260, 25);
            this.lbl_password.TabIndex = 2;
            this.lbl_password.Text = "Password";
            this.lbl_password.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_password.UseMnemonic = false;
            // 
            // lbl_space01
            // 
            this.lbl_space01.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_space01.Enabled = false;
            this.lbl_space01.Location = new System.Drawing.Point(20, 170);
            this.lbl_space01.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_space01.Name = "lbl_space01";
            this.lbl_space01.Size = new System.Drawing.Size(260, 10);
            this.lbl_space01.TabIndex = 4;
            this.lbl_space01.UseMnemonic = false;
            // 
            // lbl_space02
            // 
            this.lbl_space02.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_space02.Enabled = false;
            this.lbl_space02.Location = new System.Drawing.Point(20, 215);
            this.lbl_space02.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_space02.Name = "lbl_space02";
            this.lbl_space02.Size = new System.Drawing.Size(260, 5);
            this.lbl_space02.TabIndex = 6;
            this.lbl_space02.UseMnemonic = false;
            // 
            // tb_user_name
            // 
            // 
            // 
            // 
            this.tb_user_name.CustomButton.Image = null;
            this.tb_user_name.CustomButton.Location = new System.Drawing.Point(232, 2);
            this.tb_user_name.CustomButton.Name = "";
            this.tb_user_name.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tb_user_name.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_user_name.CustomButton.TabIndex = 1;
            this.tb_user_name.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_user_name.CustomButton.UseSelectable = true;
            this.tb_user_name.CustomButton.Visible = false;
            this.tb_user_name.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_user_name.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_user_name.Lines = new string[0];
            this.tb_user_name.Location = new System.Drawing.Point(20, 85);
            this.tb_user_name.Margin = new System.Windows.Forms.Padding(0);
            this.tb_user_name.MaxLength = 15;
            this.tb_user_name.Name = "tb_user_name";
            this.tb_user_name.PasswordChar = '\0';
            this.tb_user_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_user_name.SelectedText = "";
            this.tb_user_name.SelectionLength = 0;
            this.tb_user_name.SelectionStart = 0;
            this.tb_user_name.ShortcutsEnabled = true;
            this.tb_user_name.Size = new System.Drawing.Size(260, 30);
            this.tb_user_name.TabIndex = 1;
            this.tb_user_name.UseSelectable = true;
            this.tb_user_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_user_name.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            // 
            // tb_password
            // 
            // 
            // 
            // 
            this.tb_password.CustomButton.Image = null;
            this.tb_password.CustomButton.Location = new System.Drawing.Point(232, 2);
            this.tb_password.CustomButton.Name = "";
            this.tb_password.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tb_password.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_password.CustomButton.TabIndex = 1;
            this.tb_password.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_password.CustomButton.UseSelectable = true;
            this.tb_password.CustomButton.Visible = false;
            this.tb_password.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_password.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_password.Lines = new string[0];
            this.tb_password.Location = new System.Drawing.Point(20, 140);
            this.tb_password.Margin = new System.Windows.Forms.Padding(0);
            this.tb_password.MaxLength = 15;
            this.tb_password.Name = "tb_password";
            this.tb_password.PasswordChar = '●';
            this.tb_password.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_password.SelectedText = "";
            this.tb_password.SelectionLength = 0;
            this.tb_password.SelectionStart = 0;
            this.tb_password.ShortcutsEnabled = true;
            this.tb_password.Size = new System.Drawing.Size(260, 30);
            this.tb_password.TabIndex = 3;
            this.tb_password.UseSelectable = true;
            this.tb_password.UseSystemPasswordChar = true;
            this.tb_password.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_password.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            // 
            // btn_login
            // 
            this.btn_login.BackColor = System.Drawing.Color.OliveDrab;
            this.btn_login.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_login.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_login.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_login.ForeColor = System.Drawing.Color.White;
            this.btn_login.Location = new System.Drawing.Point(20, 180);
            this.btn_login.Margin = new System.Windows.Forms.Padding(0);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(260, 35);
            this.btn_login.TabIndex = 5;
            this.btn_login.Text = "Login";
            this.btn_login.UseCustomBackColor = true;
            this.btn_login.UseCustomForeColor = true;
            this.btn_login.UseMnemonic = false;
            this.btn_login.UseSelectable = true;
            this.btn_login.Click += new System.EventHandler(this.Btn_Login_Click);
            // 
            // btn_create_account
            // 
            this.btn_create_account.BackColor = System.Drawing.Color.RoyalBlue;
            this.btn_create_account.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_create_account.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_create_account.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_create_account.ForeColor = System.Drawing.Color.White;
            this.btn_create_account.Location = new System.Drawing.Point(20, 220);
            this.btn_create_account.Margin = new System.Windows.Forms.Padding(0);
            this.btn_create_account.Name = "btn_create_account";
            this.btn_create_account.Size = new System.Drawing.Size(260, 35);
            this.btn_create_account.TabIndex = 7;
            this.btn_create_account.Text = "Create Account";
            this.btn_create_account.UseCustomBackColor = true;
            this.btn_create_account.UseCustomForeColor = true;
            this.btn_create_account.UseMnemonic = false;
            this.btn_create_account.UseSelectable = true;
            this.btn_create_account.Click += new System.EventHandler(this.Btn_Create_Account_Click);
            // 
            // Login
            // 
            this.AcceptButton = this.btn_login;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(300, 275);
            this.Controls.Add(this.btn_create_account);
            this.Controls.Add(this.lbl_space02);
            this.Controls.Add(this.btn_login);
            this.Controls.Add(this.lbl_space01);
            this.Controls.Add(this.tb_password);
            this.Controls.Add(this.lbl_password);
            this.Controls.Add(this.tb_user_name);
            this.Controls.Add(this.lbl_user_name);
            this.Font = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Login";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "Humsee Login";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox tb_user_name;
        private MetroFramework.Controls.MetroTextBox tb_password;
        private MetroFramework.Controls.MetroButton btn_login;
        private MetroFramework.Controls.MetroButton btn_create_account;
        private MetroFramework.Controls.MetroLabel lbl_user_name;
        private MetroFramework.Controls.MetroLabel lbl_password;
        private MetroFramework.Controls.MetroLabel lbl_space01;
        private MetroFramework.Controls.MetroLabel lbl_space02;
    }
}

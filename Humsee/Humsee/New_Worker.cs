﻿using System;
using MetroFramework.Forms;

namespace Humsee
{
    public partial class New_Worker : MetroForm
    {
        public New_Worker()
        {
            InitializeComponent();
        }

        private void Btn_Create_Click(object sender, EventArgs e)
        {
            HomeUnit.AddWorker(tb_first_name.Text, tb_last_name.Text, tb_position.Text, tb_email.Text, tb_phone_number.Text);
        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            New_Worker.ActiveForm.Dispose();
        }
    }
}

﻿namespace Humsee
{
    partial class Settings
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.tb_link = new MetroFramework.Controls.MetroTextBox();
            this.lbl_link = new MetroFramework.Controls.MetroLabel();
            this.cb_language = new MetroFramework.Controls.MetroComboBox();
            this.lbl_language = new MetroFramework.Controls.MetroLabel();
            this.tb_description = new MetroFramework.Controls.MetroTextBox();
            this.lbl_description = new MetroFramework.Controls.MetroLabel();
            this.lbl_end_date = new MetroFramework.Controls.MetroLabel();
            this.tb_end_date = new MetroFramework.Controls.MetroTextBox();
            this.btn_remove = new MetroFramework.Controls.MetroButton();
            this.lbl_space03 = new MetroFramework.Controls.MetroLabel();
            this.btn_cancel = new MetroFramework.Controls.MetroButton();
            this.lbl_space02 = new MetroFramework.Controls.MetroLabel();
            this.btn_update = new MetroFramework.Controls.MetroButton();
            this.lbl_space01 = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // tb_link
            // 
            // 
            // 
            // 
            this.tb_link.CustomButton.Image = null;
            this.tb_link.CustomButton.Location = new System.Drawing.Point(232, 2);
            this.tb_link.CustomButton.Name = "";
            this.tb_link.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tb_link.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_link.CustomButton.TabIndex = 1;
            this.tb_link.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_link.CustomButton.UseSelectable = true;
            this.tb_link.CustomButton.Visible = false;
            this.tb_link.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_link.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_link.Lines = new string[0];
            this.tb_link.Location = new System.Drawing.Point(20, 196);
            this.tb_link.Margin = new System.Windows.Forms.Padding(0);
            this.tb_link.MaxLength = 50;
            this.tb_link.Name = "tb_link";
            this.tb_link.PasswordChar = '\0';
            this.tb_link.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_link.SelectedText = "";
            this.tb_link.SelectionLength = 0;
            this.tb_link.SelectionStart = 0;
            this.tb_link.ShortcutsEnabled = true;
            this.tb_link.Size = new System.Drawing.Size(260, 30);
            this.tb_link.TabIndex = 5;
            this.tb_link.UseSelectable = true;
            this.tb_link.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_link.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            // 
            // lbl_link
            // 
            this.lbl_link.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_link.Enabled = false;
            this.lbl_link.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_link.Location = new System.Drawing.Point(20, 171);
            this.lbl_link.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_link.Name = "lbl_link";
            this.lbl_link.Size = new System.Drawing.Size(260, 25);
            this.lbl_link.TabIndex = 4;
            this.lbl_link.Text = "Link";
            this.lbl_link.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_link.UseMnemonic = false;
            // 
            // cb_language
            // 
            this.cb_language.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb_language.Dock = System.Windows.Forms.DockStyle.Top;
            this.cb_language.DropDownHeight = 125;
            this.cb_language.FormattingEnabled = true;
            this.cb_language.IntegralHeight = false;
            this.cb_language.ItemHeight = 23;
            this.cb_language.Items.AddRange(new object[] {
            "C",
            "C#",
            "C++",
            "Go",
            "HTML/CSS",
            "Java",
            "JavaScript",
            "Objective-C",
            "Perl",
            "PHP",
            "Python",
            "Ruby",
            "Swift",
            "Other"});
            this.cb_language.Location = new System.Drawing.Point(20, 142);
            this.cb_language.Margin = new System.Windows.Forms.Padding(0);
            this.cb_language.MaxDropDownItems = 5;
            this.cb_language.Name = "cb_language";
            this.cb_language.Size = new System.Drawing.Size(260, 29);
            this.cb_language.TabIndex = 3;
            this.cb_language.UseSelectable = true;
            // 
            // lbl_language
            // 
            this.lbl_language.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_language.Enabled = false;
            this.lbl_language.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_language.Location = new System.Drawing.Point(20, 117);
            this.lbl_language.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_language.Name = "lbl_language";
            this.lbl_language.Size = new System.Drawing.Size(260, 25);
            this.lbl_language.TabIndex = 2;
            this.lbl_language.Text = "Language";
            this.lbl_language.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_language.UseMnemonic = false;
            // 
            // tb_description
            // 
            // 
            // 
            // 
            this.tb_description.CustomButton.Image = null;
            this.tb_description.CustomButton.Location = new System.Drawing.Point(230, 2);
            this.tb_description.CustomButton.Name = "";
            this.tb_description.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.tb_description.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_description.CustomButton.TabIndex = 1;
            this.tb_description.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_description.CustomButton.UseSelectable = true;
            this.tb_description.CustomButton.Visible = false;
            this.tb_description.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_description.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_description.Lines = new string[0];
            this.tb_description.Location = new System.Drawing.Point(20, 85);
            this.tb_description.Margin = new System.Windows.Forms.Padding(0);
            this.tb_description.MaxLength = 100;
            this.tb_description.Multiline = true;
            this.tb_description.Name = "tb_description";
            this.tb_description.PasswordChar = '\0';
            this.tb_description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_description.SelectedText = "";
            this.tb_description.SelectionLength = 0;
            this.tb_description.SelectionStart = 0;
            this.tb_description.ShortcutsEnabled = true;
            this.tb_description.Size = new System.Drawing.Size(260, 32);
            this.tb_description.TabIndex = 1;
            this.tb_description.UseSelectable = true;
            this.tb_description.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_description.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            // 
            // lbl_description
            // 
            this.lbl_description.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_description.Enabled = false;
            this.lbl_description.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_description.Location = new System.Drawing.Point(20, 60);
            this.lbl_description.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_description.Name = "lbl_description";
            this.lbl_description.Size = new System.Drawing.Size(260, 25);
            this.lbl_description.TabIndex = 0;
            this.lbl_description.Text = "Description";
            this.lbl_description.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_description.UseMnemonic = false;
            // 
            // lbl_end_date
            // 
            this.lbl_end_date.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_end_date.Enabled = false;
            this.lbl_end_date.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_end_date.Location = new System.Drawing.Point(20, 226);
            this.lbl_end_date.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_end_date.Name = "lbl_end_date";
            this.lbl_end_date.Size = new System.Drawing.Size(260, 25);
            this.lbl_end_date.TabIndex = 6;
            this.lbl_end_date.Text = "End Date";
            this.lbl_end_date.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_end_date.UseMnemonic = false;
            // 
            // tb_end_date
            // 
            // 
            // 
            // 
            this.tb_end_date.CustomButton.Image = null;
            this.tb_end_date.CustomButton.Location = new System.Drawing.Point(232, 2);
            this.tb_end_date.CustomButton.Name = "";
            this.tb_end_date.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tb_end_date.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_end_date.CustomButton.TabIndex = 1;
            this.tb_end_date.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_end_date.CustomButton.UseSelectable = true;
            this.tb_end_date.CustomButton.Visible = false;
            this.tb_end_date.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_end_date.Enabled = false;
            this.tb_end_date.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_end_date.Lines = new string[0];
            this.tb_end_date.Location = new System.Drawing.Point(20, 251);
            this.tb_end_date.Margin = new System.Windows.Forms.Padding(0);
            this.tb_end_date.MaxLength = 50;
            this.tb_end_date.Name = "tb_end_date";
            this.tb_end_date.PasswordChar = '\0';
            this.tb_end_date.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_end_date.SelectedText = "";
            this.tb_end_date.SelectionLength = 0;
            this.tb_end_date.SelectionStart = 0;
            this.tb_end_date.ShortcutsEnabled = true;
            this.tb_end_date.Size = new System.Drawing.Size(260, 30);
            this.tb_end_date.TabIndex = 7;
            this.tb_end_date.UseSelectable = true;
            this.tb_end_date.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_end_date.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            // 
            // btn_remove
            // 
            this.btn_remove.BackColor = System.Drawing.Color.DimGray;
            this.btn_remove.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_remove.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_remove.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_remove.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_remove.ForeColor = System.Drawing.Color.White;
            this.btn_remove.Location = new System.Drawing.Point(20, 376);
            this.btn_remove.Margin = new System.Windows.Forms.Padding(0);
            this.btn_remove.Name = "btn_remove";
            this.btn_remove.Size = new System.Drawing.Size(260, 35);
            this.btn_remove.TabIndex = 13;
            this.btn_remove.Text = "Remove";
            this.btn_remove.UseCustomBackColor = true;
            this.btn_remove.UseCustomForeColor = true;
            this.btn_remove.UseMnemonic = false;
            this.btn_remove.UseSelectable = true;
            this.btn_remove.Click += new System.EventHandler(this.Btn_Remove_Click);
            // 
            // lbl_space03
            // 
            this.lbl_space03.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_space03.Enabled = false;
            this.lbl_space03.Location = new System.Drawing.Point(20, 366);
            this.lbl_space03.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_space03.Name = "lbl_space03";
            this.lbl_space03.Size = new System.Drawing.Size(260, 10);
            this.lbl_space03.TabIndex = 12;
            this.lbl_space03.UseMnemonic = false;
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackColor = System.Drawing.Color.Crimson;
            this.btn_cancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_cancel.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_cancel.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_cancel.ForeColor = System.Drawing.Color.White;
            this.btn_cancel.Location = new System.Drawing.Point(20, 331);
            this.btn_cancel.Margin = new System.Windows.Forms.Padding(0);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(260, 35);
            this.btn_cancel.TabIndex = 11;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseCustomBackColor = true;
            this.btn_cancel.UseCustomForeColor = true;
            this.btn_cancel.UseMnemonic = false;
            this.btn_cancel.UseSelectable = true;
            this.btn_cancel.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // lbl_space02
            // 
            this.lbl_space02.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_space02.Enabled = false;
            this.lbl_space02.Location = new System.Drawing.Point(20, 326);
            this.lbl_space02.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_space02.Name = "lbl_space02";
            this.lbl_space02.Size = new System.Drawing.Size(260, 5);
            this.lbl_space02.TabIndex = 10;
            this.lbl_space02.UseMnemonic = false;
            // 
            // btn_update
            // 
            this.btn_update.BackColor = System.Drawing.Color.OliveDrab;
            this.btn_update.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_update.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_update.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_update.ForeColor = System.Drawing.Color.White;
            this.btn_update.Location = new System.Drawing.Point(20, 291);
            this.btn_update.Margin = new System.Windows.Forms.Padding(0);
            this.btn_update.Name = "btn_update";
            this.btn_update.Size = new System.Drawing.Size(260, 35);
            this.btn_update.TabIndex = 9;
            this.btn_update.Text = "Update";
            this.btn_update.UseCustomBackColor = true;
            this.btn_update.UseCustomForeColor = true;
            this.btn_update.UseMnemonic = false;
            this.btn_update.UseSelectable = true;
            this.btn_update.Click += new System.EventHandler(this.Btn_Update_Click);
            // 
            // lbl_space01
            // 
            this.lbl_space01.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_space01.Enabled = false;
            this.lbl_space01.Location = new System.Drawing.Point(20, 281);
            this.lbl_space01.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_space01.Name = "lbl_space01";
            this.lbl_space01.Size = new System.Drawing.Size(260, 10);
            this.lbl_space01.TabIndex = 8;
            this.lbl_space01.UseMnemonic = false;
            // 
            // Settings
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(300, 426);
            this.ControlBox = false;
            this.Controls.Add(this.btn_remove);
            this.Controls.Add(this.lbl_space03);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.lbl_space02);
            this.Controls.Add(this.btn_update);
            this.Controls.Add(this.lbl_space01);
            this.Controls.Add(this.tb_end_date);
            this.Controls.Add(this.lbl_end_date);
            this.Controls.Add(this.tb_link);
            this.Controls.Add(this.lbl_link);
            this.Controls.Add(this.cb_language);
            this.Controls.Add(this.lbl_language);
            this.Controls.Add(this.tb_description);
            this.Controls.Add(this.lbl_description);
            this.Font = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Movable = false;
            this.Name = "Settings";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "Settings";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Activated += new System.EventHandler(this.Settings_Activated);
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroTextBox tb_link;
        private MetroFramework.Controls.MetroLabel lbl_link;
        private MetroFramework.Controls.MetroComboBox cb_language;
        private MetroFramework.Controls.MetroLabel lbl_language;
        private MetroFramework.Controls.MetroTextBox tb_description;
        private MetroFramework.Controls.MetroLabel lbl_description;
        private MetroFramework.Controls.MetroLabel lbl_end_date;
        private MetroFramework.Controls.MetroTextBox tb_end_date;
        private MetroFramework.Controls.MetroButton btn_remove;
        private MetroFramework.Controls.MetroLabel lbl_space03;
        private MetroFramework.Controls.MetroButton btn_cancel;
        private MetroFramework.Controls.MetroLabel lbl_space02;
        private MetroFramework.Controls.MetroButton btn_update;
        private MetroFramework.Controls.MetroLabel lbl_space01;
    }
}
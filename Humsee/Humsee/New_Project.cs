﻿using System;
using MetroFramework.Forms;

namespace Humsee
{
    public partial class New_Project : MetroForm
    {
        public New_Project()
        {
            InitializeComponent();
        }

        private void New_Project_Load(object sender, EventArgs e)
        {
            // Min Date Set
            dt_end.MinDate = DateTime.Now;
        }

        private void Btn_Create_Click(object sender, EventArgs e)
        {
            ProjectUnit.Create(tb_name.Text, tb_description.Text, cb_language.Text, tb_link.Text, dt_end.Text);
        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            New_Project.ActiveForm.Dispose();
        }
    }
}

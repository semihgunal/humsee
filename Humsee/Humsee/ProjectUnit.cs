﻿using MetroFramework;
using System.Windows.Forms;

namespace Humsee
{
    class ProjectUnit
    {
        public static string UserName = null;
        public static void Create(string name, string description, string language, string link, string end)
        {
            // [Code 002] Input Null Test
            if (name != "" && language != "")
            {
                // [Code 003] Input Space Test
                if (name.IndexOf(" ") < 0 && language.IndexOf(" ") < 0)
                {
                    DataBaseUnit.ProjectInsert(name, description, language, link, end);
                    MetroMessageBox.Show(Project.ActiveForm, "New project has been successfully created", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Project.ActiveForm.Dispose();
                }
                else
                {
                    MetroMessageBox.Show(Project.ActiveForm, "inputs can not contain space", "Error! [Code 003]", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MetroMessageBox.Show(Project.ActiveForm, "inputs can not contain null value", "Error! [Code 002]", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
    }
}

﻿namespace Humsee
{
    partial class New_Task
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lbl_select_worker = new MetroFramework.Controls.MetroLabel();
            this.lbl_task = new MetroFramework.Controls.MetroLabel();
            this.lbl_end_date = new MetroFramework.Controls.MetroLabel();
            this.lbl_space02 = new MetroFramework.Controls.MetroLabel();
            this.lbl_space01 = new MetroFramework.Controls.MetroLabel();
            this.cb_select_worker = new MetroFramework.Controls.MetroComboBox();
            this.workersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.humseeDataSetWorkers = new Humsee.HumseeDataSetWorkers();
            this.tb_task = new MetroFramework.Controls.MetroTextBox();
            this.dt_end = new MetroFramework.Controls.MetroDateTime();
            this.btn_cancel = new MetroFramework.Controls.MetroButton();
            this.btn_create = new MetroFramework.Controls.MetroButton();
            this.workersTableAdapter = new Humsee.HumseeDataSetWorkersTableAdapters.WorkersTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.workersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.humseeDataSetWorkers)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_select_worker
            // 
            this.lbl_select_worker.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_select_worker.Enabled = false;
            this.lbl_select_worker.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_select_worker.Location = new System.Drawing.Point(20, 60);
            this.lbl_select_worker.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_select_worker.Name = "lbl_select_worker";
            this.lbl_select_worker.Size = new System.Drawing.Size(260, 25);
            this.lbl_select_worker.TabIndex = 0;
            this.lbl_select_worker.Text = "Select Worker";
            this.lbl_select_worker.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_select_worker.UseMnemonic = false;
            // 
            // lbl_task
            // 
            this.lbl_task.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_task.Enabled = false;
            this.lbl_task.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_task.Location = new System.Drawing.Point(20, 114);
            this.lbl_task.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_task.Name = "lbl_task";
            this.lbl_task.Size = new System.Drawing.Size(260, 25);
            this.lbl_task.TabIndex = 2;
            this.lbl_task.Text = "Task";
            this.lbl_task.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_task.UseMnemonic = false;
            // 
            // lbl_end_date
            // 
            this.lbl_end_date.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_end_date.Enabled = false;
            this.lbl_end_date.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_end_date.Location = new System.Drawing.Point(20, 281);
            this.lbl_end_date.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_end_date.Name = "lbl_end_date";
            this.lbl_end_date.Size = new System.Drawing.Size(260, 25);
            this.lbl_end_date.TabIndex = 4;
            this.lbl_end_date.Text = "End Date";
            this.lbl_end_date.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_end_date.UseMnemonic = false;
            // 
            // lbl_space02
            // 
            this.lbl_space02.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_space02.Enabled = false;
            this.lbl_space02.Location = new System.Drawing.Point(20, 380);
            this.lbl_space02.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_space02.Name = "lbl_space02";
            this.lbl_space02.Size = new System.Drawing.Size(260, 5);
            this.lbl_space02.TabIndex = 8;
            this.lbl_space02.UseMnemonic = false;
            // 
            // lbl_space01
            // 
            this.lbl_space01.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_space01.Enabled = false;
            this.lbl_space01.Location = new System.Drawing.Point(20, 335);
            this.lbl_space01.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_space01.Name = "lbl_space01";
            this.lbl_space01.Size = new System.Drawing.Size(260, 10);
            this.lbl_space01.TabIndex = 6;
            this.lbl_space01.UseMnemonic = false;
            // 
            // cb_select_worker
            // 
            this.cb_select_worker.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb_select_worker.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.workersBindingSource, "Email", true));
            this.cb_select_worker.DataSource = this.workersBindingSource;
            this.cb_select_worker.DisplayMember = "Email";
            this.cb_select_worker.Dock = System.Windows.Forms.DockStyle.Top;
            this.cb_select_worker.DropDownHeight = 125;
            this.cb_select_worker.FormattingEnabled = true;
            this.cb_select_worker.IntegralHeight = false;
            this.cb_select_worker.ItemHeight = 23;
            this.cb_select_worker.Location = new System.Drawing.Point(20, 85);
            this.cb_select_worker.Margin = new System.Windows.Forms.Padding(0);
            this.cb_select_worker.MaxDropDownItems = 5;
            this.cb_select_worker.Name = "cb_select_worker";
            this.cb_select_worker.Size = new System.Drawing.Size(260, 29);
            this.cb_select_worker.TabIndex = 1;
            this.cb_select_worker.UseSelectable = true;
            this.cb_select_worker.ValueMember = "Email";
            // 
            // workersBindingSource
            // 
            this.workersBindingSource.DataMember = "Workers";
            this.workersBindingSource.DataSource = this.humseeDataSetWorkers;
            // 
            // humseeDataSetWorkers
            // 
            this.humseeDataSetWorkers.DataSetName = "HumseeDataSetWorkers";
            this.humseeDataSetWorkers.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tb_task
            // 
            this.tb_task.CustomButton.Image = null;
            this.tb_task.CustomButton.Location = new System.Drawing.Point(120, 2);
            this.tb_task.CustomButton.Name = "";
            this.tb_task.CustomButton.Size = new System.Drawing.Size(137, 137);
            this.tb_task.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_task.CustomButton.TabIndex = 1;
            this.tb_task.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_task.CustomButton.UseSelectable = true;
            this.tb_task.CustomButton.Visible = false;
            this.tb_task.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_task.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_task.Lines = new string[0];
            this.tb_task.Location = new System.Drawing.Point(20, 139);
            this.tb_task.Margin = new System.Windows.Forms.Padding(0);
            this.tb_task.MaxLength = 100;
            this.tb_task.Multiline = true;
            this.tb_task.Name = "tb_task";
            this.tb_task.PasswordChar = '\0';
            this.tb_task.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_task.SelectedText = "";
            this.tb_task.SelectionLength = 0;
            this.tb_task.SelectionStart = 0;
            this.tb_task.ShortcutsEnabled = true;
            this.tb_task.Size = new System.Drawing.Size(260, 142);
            this.tb_task.TabIndex = 3;
            this.tb_task.UseSelectable = true;
            this.tb_task.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_task.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            // 
            // dt_end
            // 
            this.dt_end.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dt_end.CustomFormat = "dd.MM.yyyy";
            this.dt_end.Dock = System.Windows.Forms.DockStyle.Top;
            this.dt_end.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dt_end.Location = new System.Drawing.Point(20, 306);
            this.dt_end.Margin = new System.Windows.Forms.Padding(0);
            this.dt_end.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dt_end.MinimumSize = new System.Drawing.Size(0, 29);
            this.dt_end.Name = "dt_end";
            this.dt_end.Size = new System.Drawing.Size(260, 29);
            this.dt_end.TabIndex = 5;
            this.dt_end.Value = new System.DateTime(2017, 12, 19, 0, 0, 0, 0);
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackColor = System.Drawing.Color.Crimson;
            this.btn_cancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_cancel.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_cancel.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_cancel.ForeColor = System.Drawing.Color.White;
            this.btn_cancel.Location = new System.Drawing.Point(20, 385);
            this.btn_cancel.Margin = new System.Windows.Forms.Padding(0);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(260, 35);
            this.btn_cancel.TabIndex = 9;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseCustomBackColor = true;
            this.btn_cancel.UseCustomForeColor = true;
            this.btn_cancel.UseMnemonic = false;
            this.btn_cancel.UseSelectable = true;
            this.btn_cancel.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // btn_create
            // 
            this.btn_create.BackColor = System.Drawing.Color.OliveDrab;
            this.btn_create.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_create.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_create.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_create.ForeColor = System.Drawing.Color.White;
            this.btn_create.Location = new System.Drawing.Point(20, 345);
            this.btn_create.Margin = new System.Windows.Forms.Padding(0);
            this.btn_create.Name = "btn_create";
            this.btn_create.Size = new System.Drawing.Size(260, 35);
            this.btn_create.TabIndex = 7;
            this.btn_create.Text = "Create";
            this.btn_create.UseCustomBackColor = true;
            this.btn_create.UseCustomForeColor = true;
            this.btn_create.UseMnemonic = false;
            this.btn_create.UseSelectable = true;
            this.btn_create.Click += new System.EventHandler(this.Btn_Create_Click);
            // 
            // workersTableAdapter
            // 
            this.workersTableAdapter.ClearBeforeFill = true;
            // 
            // New_Task
            // 
            this.AcceptButton = this.btn_create;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton = this.btn_cancel;
            this.ClientSize = new System.Drawing.Size(300, 440);
            this.ControlBox = false;
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.lbl_space02);
            this.Controls.Add(this.btn_create);
            this.Controls.Add(this.lbl_space01);
            this.Controls.Add(this.dt_end);
            this.Controls.Add(this.lbl_end_date);
            this.Controls.Add(this.tb_task);
            this.Controls.Add(this.lbl_task);
            this.Controls.Add(this.cb_select_worker);
            this.Controls.Add(this.lbl_select_worker);
            this.Font = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Movable = false;
            this.Name = "New_Task";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "New Task";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.New_Task_Load);
            ((System.ComponentModel.ISupportInitialize)(this.workersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.humseeDataSetWorkers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroComboBox cb_select_worker;
        private MetroFramework.Controls.MetroTextBox tb_task;
        private MetroFramework.Controls.MetroDateTime dt_end;
        private MetroFramework.Controls.MetroButton btn_cancel;
        private MetroFramework.Controls.MetroButton btn_create;
        private MetroFramework.Controls.MetroLabel lbl_select_worker;
        private MetroFramework.Controls.MetroLabel lbl_task;
        private MetroFramework.Controls.MetroLabel lbl_end_date;
        private MetroFramework.Controls.MetroLabel lbl_space02;
        private MetroFramework.Controls.MetroLabel lbl_space01;
        private HumseeDataSetWorkers humseeDataSetWorkers;
        private System.Windows.Forms.BindingSource workersBindingSource;
        private HumseeDataSetWorkersTableAdapters.WorkersTableAdapter workersTableAdapter;
    }
}
﻿using System;
using System.Windows.Forms;
using MetroFramework;
using MetroFramework.Forms;

namespace Humsee
{
    public partial class Expiring_Project : MetroForm
    {
        public Expiring_Project()
        {
            InitializeComponent();
        }

        private void Expiring_Project_Load(object sender, EventArgs e)
        {
            // Min Date Set
            dt_new_end.MinDate = DateTime.Now;
            // Project Name Set
            tb_project_name.Text = DataBaseUnit.ProjectTable[0];
        }

        private void Btn_Extend_Date_Click(object sender, EventArgs e)
        {
            DataBaseUnit.ProjectEndDateUpdate(dt_new_end.Text);
            MetroMessageBox.Show(New_Worker.ActiveForm, " ", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Expiring_Project.ActiveForm.Dispose();
        }

        private void Btn_Ignore_Click(object sender, EventArgs e)
        {
            Expiring_Project.ActiveForm.Dispose();
        }
    }
}

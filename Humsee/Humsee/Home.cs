﻿using System;
using MetroFramework;
using MetroFramework.Forms;
using System.Windows.Forms;

namespace Humsee
{
    public partial class Home : MetroForm
    {
        public Home()
        {
            InitializeComponent();
        }

        private void Home_Load(object sender, EventArgs e)
        {
            Tool_Tip_Load();
            // Project Info Read
            DataBaseUnit.ProjectRead();
            // Expring Project Test
            DateTime Last_Date = DateTime.ParseExact(DataBaseUnit.ProjectTable[4], "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);

            if (Last_Date < DateTime.Today)
            {
                Expiring_Project Form = new Expiring_Project();
                Form.ShowDialog();
            }
        }

        private void Home_Activated(object sender, EventArgs e)
        {
            Home_Refresh();
        }

        private void Home_Refresh ()
        {
            // Project Table Read
            DataBaseUnit.ProjectRead();
            // Project Name Show
            lbl_project_name.Text = DataBaseUnit.ProjectTable[0];
            // Data Base Fill
            this.activityTableAdapter.Fill(this.humseeDataSetActivity.Activity, DataBaseUnit.ProjectTable[0]);
            this.tasksTableAdapter.Fill(this.humseeDataSetTasks.Tasks, DataBaseUnit.ProjectTable[0]);
            this.workersTableAdapter.Fill(this.humseeDataSetWorkers.Workers, DataBaseUnit.ProjectTable[0]);
        }

        private void Tool_Tip_Load()
        {
            // Tool Tip Variable
            ToolTip TT_Activity_Log = new ToolTip();
            ToolTip TT_Task_Add = new ToolTip();
            ToolTip TT_Tasks = new ToolTip();
            ToolTip TT_Worker_Add = new ToolTip();
            ToolTip TT_Workers = new ToolTip();
            ToolTip TT_Link = new ToolTip();
            ToolTip TT_Settings = new ToolTip();
            ToolTip TT_Exit = new ToolTip();
            ToolTip TT_Logout = new ToolTip();
            // Tool Tip Set
            TT_Activity_Log.SetToolTip(btn_activity_log, "Activity Log");
            TT_Task_Add.SetToolTip(btn_task_add, "Task Add");
            TT_Tasks.SetToolTip(btn_tasks, "Tasks");
            TT_Worker_Add.SetToolTip(btn_worker_add, "Worker Add");
            TT_Workers.SetToolTip(btn_workers, "Workers");
            TT_Link.SetToolTip(btn_link, "Link");
            TT_Settings.SetToolTip(btn_settings, "Settings");
            TT_Exit.SetToolTip(btn_exit, "Project Close");
            TT_Logout.SetToolTip(btn_logout, "Logout");
        }

        // Left Panel Buttons Click Event
        private void Btn_Activity_Log_Click(object sender, EventArgs e)
        {
            // Tab Select
            tab_control.SelectedTab = tab_activity_log;
        }

        private void Btn_Task_Add_Click(object sender, EventArgs e)
        {
            New_Task Form = new New_Task();
            Form.ShowDialog();
        }

        private void Btn_Tasks_Click(object sender, EventArgs e)
        {
            // Tab Select
            tab_control.SelectedTab = tab_tasks;
        }

        private void Btn_User_Add_Click(object sender, EventArgs e)
        {
            New_Worker Form = new New_Worker();
            Form.ShowDialog();
        }

        private void Btn_Users_Click(object sender, EventArgs e)
        {
            // Tab Select
            tab_control.SelectedTab = tab_workers;
        }

        private void Btn_Link_Click(object sender, EventArgs e)
        {
            // Link Open in Browser
            System.Diagnostics.Process.Start(DataBaseUnit.ProjectTable[3]);
        }

        private void Btn_Settings_Click(object sender, EventArgs e)
        {
            Settings Form = new Settings();
            Form.ShowDialog();
        }

        private void Btn_Exit_Click(object sender, EventArgs e)
        {
            Home.ActiveForm.Dispose();
        }

        private void Btn_Logout_Click(object sender, EventArgs e)
        {
            Application.Restart();
        }

        // Bottom Panel Buttons Click Event
        private void Btn_Remove_Worker_Click(object sender, EventArgs e)
        {
            // [Code 002] Input Null Test
            if (cb_select_worker.Text != "")
            {
                HomeUnit.RemoveWorker(cb_select_worker.Text);
            }
            else
            {
                MetroMessageBox.Show(New_Worker.ActiveForm, "inputs can not contain null value", "Error! [Code 002]", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void Btn_Remove_Task_Click(object sender, EventArgs e)
        {
            // [Code 002] Input Null Test
            if (cb_select_task.Text != "")
            {
                HomeUnit.RemoveTask(cb_select_task.Text);
            }
            else
            {
                MetroMessageBox.Show(New_Worker.ActiveForm, "inputs can not contain null value", "Error! [Code 002]", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        private void Btn_Done_Task_Click(object sender, EventArgs e)
        {
            // [Code 002] Input Null Test
            if (cb_select_task.Text != "")
            {
                HomeUnit.DoneTask(cb_select_task.Text);
            }
            else
            {
                MetroMessageBox.Show(New_Worker.ActiveForm, "inputs can not contain null value", "Error! [Code 002]", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
    }
}

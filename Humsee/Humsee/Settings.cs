﻿using System;
using MetroFramework;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace Humsee
{
    public partial class Settings : MetroForm
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void Settings_Refresh()
        {
            HomeUnit.SetProjectSettings(tb_description, cb_language, tb_link, tb_end_date);
        }

        private void Btn_Update_Click(object sender, EventArgs e)
        {
            HomeUnit.UpdateProject(tb_description.Text, cb_language.Text, tb_link.Text);
        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            Settings.ActiveForm.Dispose();
        }

        private void Btn_Remove_Click(object sender, EventArgs e)
        {
            DataBaseUnit.ProjectDelete();
            MetroMessageBox.Show(Settings.ActiveForm, "Project has been successfully removed", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
            // Logout
            Application.Restart();
        }

        private void Settings_Activated(object sender, EventArgs e)
        {
            Settings_Refresh();
        }
    }
}

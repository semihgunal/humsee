﻿using MetroFramework;
using System.Windows.Forms;

namespace Humsee
{
    class MemberUnit
    {
        public static bool Login(string uname, string password)
        {
            // [Code 000] Input Null Test
            if (uname != "" && password != "")
            {
                // [Code 001] Username and Password Match Test
                if (DataBaseUnit.MemberPasswordRead(uname) == password)
                {
                    return true;
                }
                else
                {
                    MetroMessageBox.Show(Humsee.Login.ActiveForm, "username and password do not match", "Error! [Code 001]", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return false;
                }
            }
            else
            {
                MetroMessageBox.Show(Humsee.Login.ActiveForm, "username or password can not contain null value", "Error! [Code 000]", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return false;
            }
        }

        public static void Register(string uname, string fname, string lname, string password, string cpassword)
        {
            // Test Variables
            bool Input_Null_Test = (uname != "" && fname != "" && lname != "" && password != "" && cpassword != "");
            bool Input_Space_Test = (uname.IndexOf(" ") < 0 && fname.IndexOf(" ") < 0 && lname.IndexOf(" ") < 0 && password.IndexOf(" ") < 0 && cpassword.IndexOf(" ") < 0);
            
            // [Code 002] Input Null Test
            if (Input_Null_Test)
            {
                // [Code 003] Input Space Test
                if (Input_Space_Test)
                {
                    // [Code 004] Password and Confirm Password Match Test
                    if (password == cpassword)
                    {
                        DataBaseUnit.MemberInsert(uname, fname, lname, password);
                        MetroMessageBox.Show(Humsee.Register.ActiveForm, "New user has been successfully created", "Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Humsee.Register.ActiveForm.Dispose();
                    }
                    else
                    {
                        MetroMessageBox.Show(Humsee.Register.ActiveForm, "passwords do not match", "Error! [Code 004]", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
                else
                {
                    MetroMessageBox.Show(Humsee.Register.ActiveForm, "inputs can not contain space", "Error! [Code 003]", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            else
            {
                MetroMessageBox.Show(Humsee.Register.ActiveForm, "inputs can not contain null value", "Error! [Code 002]", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
    }
}

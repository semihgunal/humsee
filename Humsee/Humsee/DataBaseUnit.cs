﻿using System;
using MetroFramework;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Humsee
{
    class DataBaseUnit
    {
        // Connect Value
        private static string DataSource = "DESKTOP-6GEGC18\\SQLEXPRESS";
        private static string InitialCatalog = "Humsee";
        // SQL Variable
        private static SqlConnection Connection;
        private static SqlCommand Command;
        // Member Password Read Value
        private static string Password = null;
        private static string Message = null;
        // 0: Name - 1: Description - 2: Language - 3: Link - 4: End Date
        public static string [] ProjectTable = new string[5] {null, null, null, null, null};

        public static void Connect(string dsource, string icatalog)
        {
            Connection = new SqlConnection($"Data Source = {dsource}; Initial Catalog = {icatalog}; Integrated Security = True");

            try
            {
                Connection.Open();
            }
            catch (Exception Err)
            {
                MetroMessageBox.Show(New_Task.ActiveForm, Err.ToString(), "Connection Error!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }

        public static string MemberPasswordRead(string uname)
        {
            // Connect
            Connect(DataSource, InitialCatalog);
            // Member Password Read Command
            Command = new SqlCommand($"SELECT * FROM Members WHERE [User Name] = '{uname}'", Connection);
            SqlDataReader DataReader = Command.ExecuteReader();
            while (DataReader.Read())
            {
                Password = DataReader["Password"].ToString();
            }
            // Disconnect
            Connection.Close();
            // Return
            return Password;
        }

        public static void MemberInsert(string uname, string fname, string lname, string password)
        {
            // Connect
            Connect(DataSource, InitialCatalog);
            // Member Insert Command
            Command = new SqlCommand($"INSERT INTO Members([User Name], [First Name], [Last Name], Password) VALUES ('{uname}','{fname}','{lname}','{password}')", Connection);
            Command.ExecuteNonQuery();
            // Disconnect
            Connection.Close();
        }

        public static void ProjectInsert(string name, string description, string language, string link, string end)
        {
            // Connect
            Connect(DataSource, InitialCatalog);
            // Project Insert Command
            Command = new SqlCommand($"INSERT INTO Projects(Name, Description, Language, Link, [Start Date], [End Date], [User Name]) VALUES ('{name}','{description}','{language}','{link}','{DateTime.Now.ToString("dd/MM/yyyy")}','{end}','{ProjectUnit.UserName}')", Connection);
            Command.ExecuteNonQuery();
            // Disconnect
            Connection.Close();
        }

        public static void ProjectDelete()
        {
            // Connect
            Connect(DataSource, InitialCatalog);
            // Project Delete Command
            Command = new SqlCommand($"DELETE FROM Projects WHERE Name ='{ProjectTable[0]}'", Connection);
            Command.ExecuteNonQuery();
            Command = new SqlCommand($"DELETE FROM Activity WHERE [Project Name] ='{ProjectTable[0]}'", Connection);
            Command.ExecuteNonQuery();
            Command = new SqlCommand($"DELETE FROM Tasks WHERE [Project Name] ='{ProjectTable[0]}'", Connection);
            Command.ExecuteNonQuery();
            Command = new SqlCommand($"DELETE FROM Workers WHERE [Project Name] ='{ProjectTable[0]}'", Connection);
            Command.ExecuteNonQuery();
            // Disconnect
            Connection.Close();
        }

        public static void ProjectRead()
        {
            // Connect
            Connect(DataSource, InitialCatalog);
            // Project Table Read Command
            Command = new SqlCommand($"SELECT * FROM Projects WHERE [Name] = '{ProjectTable[0]}'", Connection);
            SqlDataReader DataReader = Command.ExecuteReader();
            while (DataReader.Read())
            {
                //ProjectTable[0] = DataReader["Name"].ToString();
                ProjectTable[1] = DataReader["Description"].ToString();
                ProjectTable[2] = DataReader["Language"].ToString();
                ProjectTable[3] = DataReader["Link"].ToString();
                ProjectTable[4] = DataReader["End Date"].ToString();
            }
            // Disconnect
            Connection.Close();
        }

        public static void ProjectUpdate(string description, string language, string link)
        {
            // Connect
            Connect(DataSource, InitialCatalog);
            // Project Update Command
            Command = new SqlCommand($"UPDATE Projects SET Description='{description}', Language='{language}', Link='{link}' WHERE Name = '{ProjectTable[0]}'", Connection);
            Command.ExecuteNonQuery();
            // Disconnect
            Connection.Close();
        }

        public static void ProjectEndDateUpdate(string end)
        {
            // Connect
            Connect(DataSource, InitialCatalog);
            // Project Update Command
            Command = new SqlCommand($"UPDATE Projects SET [End Date] = '{end}' WHERE Name = '{ProjectTable[0]}'", Connection);
            Command.ExecuteNonQuery();
            // Disconnect
            Connection.Close();
        }

        public static void WorkerInsert(string fname, string lname, string position, string email, string phone)
        {
            // Connect
            Connect(DataSource, InitialCatalog);
            // Worker Insert Command
            Command = new SqlCommand($"INSERT INTO Workers([First Name], [Last Name], Position, Email, [Phone Number], [Project Name]) VALUES ('{fname}','{lname}','{position}','{email}','{phone}','{ProjectTable[0]}')", Connection);
            Command.ExecuteNonQuery();
            // Disconnect
            Connection.Close();
            // Activity Log Insert
            Message = fname + " " + lname + " added as a " + position;
            ActivityInsert("Add", Message);
        }

        public static void WorkerRemove(string worker)
        {
            // Connect
            Connect(DataSource, InitialCatalog);
            // Worker Remove Command
            Command = new SqlCommand($"DELETE FROM Workers WHERE Email ='{worker}'", Connection);
            Command.ExecuteNonQuery();
            // Disconnect
            Connection.Close();
            // Activity Log Insert
            Message = worker + " deleted from workers";
            ActivityInsert("Remove", Message);
        }

        public static void TaskInsert(string worker, string task, string tstatus, string end)
        {
            // Connect
            Connect(DataSource, InitialCatalog);
            // Task Insert Command
            Command = new SqlCommand($"INSERT INTO Tasks(Worker, Task, [Task Status], [Start Date], [End Date], [Project Name]) VALUES ('{worker}','{task}','{tstatus}','{DateTime.Now.ToString("dd/MM/yyyy")}','{end}','{ProjectTable[0]}')", Connection);
            Command.ExecuteNonQuery();
            // Disconnect
            Connection.Close();
            // Activity Log Insert
            Message = worker + " assigned to " + task;
            ActivityInsert("Add", Message);
            // Send Mail
            Email.SendMail(worker, task + "\nEnd Time: " + end);
        }

        public static void TaskRemove(string task)
        {
            // Connect
            Connect(DataSource, InitialCatalog);
            // Task Remove Command
            Command = new SqlCommand($"DELETE FROM Tasks WHERE Task ='{task}'", Connection);
            Command.ExecuteNonQuery();
            // Disconnect
            Connection.Close();
            // Activity Log Insert
            Message = task + " deleted from tasks"; ;
            ActivityInsert("Remove", Message);
        }

        public static void TaskDone(string task)
        {
            // Connect
            Connect(DataSource, InitialCatalog);
            // Task Done Command
            Command = new SqlCommand($"UPDATE Tasks SET [Task Status]='Done' WHERE Task = '{task}'", Connection);
            Command.ExecuteNonQuery();
            // Disconnect
            Connection.Close();
            // Activity Log Insert
            Message = task + " finished";
            ActivityInsert("Done", Message);
        }

        public static void ActivityInsert (string operation, string message)
        {
            // Connect
            Connect(DataSource, InitialCatalog);
            // Activity Insert Command
            Command = new SqlCommand($"INSERT INTO Activity(Operation, Message, Date, [Project Name]) VALUES ('{operation}','{message}','{DateTime.Now.ToString("dd/MM/yyyy")}','{ProjectTable[0]}')", Connection);
            Command.ExecuteNonQuery();
            // Disconnect
            Connection.Close();
        }
    }
}

﻿namespace Humsee
{
    partial class Home
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Home));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnl_top = new MetroFramework.Controls.MetroPanel();
            this.lbl_project_name = new MetroFramework.Controls.MetroLabel();
            this.pnl_left = new MetroFramework.Controls.MetroPanel();
            this.btn_activity_log = new MetroFramework.Controls.MetroButton();
            this.btn_task_add = new MetroFramework.Controls.MetroButton();
            this.btn_tasks = new MetroFramework.Controls.MetroButton();
            this.btn_worker_add = new MetroFramework.Controls.MetroButton();
            this.btn_workers = new MetroFramework.Controls.MetroButton();
            this.btn_link = new MetroFramework.Controls.MetroButton();
            this.btn_settings = new MetroFramework.Controls.MetroButton();
            this.btn_exit = new MetroFramework.Controls.MetroButton();
            this.btn_logout = new MetroFramework.Controls.MetroButton();
            this.tab_control = new MetroFramework.Controls.MetroTabControl();
            this.tab_activity_log = new MetroFramework.Controls.MetroTabPage();
            this.grid_activity_log = new MetroFramework.Controls.MetroGrid();
            this.operationDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.messageDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.activityBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.humseeDataSetActivity = new Humsee.HumseeDataSetActivity();
            this.tab_tasks = new MetroFramework.Controls.MetroTabPage();
            this.grid_tasks = new MetroFramework.Controls.MetroGrid();
            this.workerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.taskStatusDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.startDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.endDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tasksBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.humseeDataSetTasks = new Humsee.HumseeDataSetTasks();
            this.tab_workers = new MetroFramework.Controls.MetroTabPage();
            this.grid_workers = new MetroFramework.Controls.MetroGrid();
            this.firstNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.positionDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.emailDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.phoneNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.workersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.humseeDataSetWorkersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.humseeDataSetWorkers = new Humsee.HumseeDataSetWorkers();
            this.pnl_down = new MetroFramework.Controls.MetroPanel();
            this.lbl_select_worker = new MetroFramework.Controls.MetroLabel();
            this.cb_select_worker = new MetroFramework.Controls.MetroComboBox();
            this.btn_remove_worker = new MetroFramework.Controls.MetroButton();
            this.lbl_select_task = new MetroFramework.Controls.MetroLabel();
            this.cb_select_task = new MetroFramework.Controls.MetroComboBox();
            this.btn_done_task = new MetroFramework.Controls.MetroButton();
            this.btn_remove_task = new MetroFramework.Controls.MetroButton();
            this.tasksTableAdapter = new Humsee.HumseeDataSetTasksTableAdapters.TasksTableAdapter();
            this.workersTableAdapter = new Humsee.HumseeDataSetWorkersTableAdapters.WorkersTableAdapter();
            this.activityTableAdapter = new Humsee.HumseeDataSetActivityTableAdapters.ActivityTableAdapter();
            this.pnl_top.SuspendLayout();
            this.pnl_left.SuspendLayout();
            this.tab_control.SuspendLayout();
            this.tab_activity_log.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid_activity_log)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.activityBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.humseeDataSetActivity)).BeginInit();
            this.tab_tasks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid_tasks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tasksBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.humseeDataSetTasks)).BeginInit();
            this.tab_workers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grid_workers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.workersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.humseeDataSetWorkersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.humseeDataSetWorkers)).BeginInit();
            this.pnl_down.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnl_top
            // 
            this.pnl_top.BackColor = System.Drawing.Color.White;
            this.pnl_top.Controls.Add(this.lbl_project_name);
            this.pnl_top.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnl_top.HorizontalScrollbarBarColor = true;
            this.pnl_top.HorizontalScrollbarHighlightOnWheel = false;
            this.pnl_top.HorizontalScrollbarSize = 10;
            this.pnl_top.Location = new System.Drawing.Point(20, 60);
            this.pnl_top.Margin = new System.Windows.Forms.Padding(0);
            this.pnl_top.Name = "pnl_top";
            this.pnl_top.Size = new System.Drawing.Size(760, 40);
            this.pnl_top.TabIndex = 0;
            this.pnl_top.VerticalScrollbarBarColor = true;
            this.pnl_top.VerticalScrollbarHighlightOnWheel = false;
            this.pnl_top.VerticalScrollbarSize = 10;
            // 
            // lbl_project_name
            // 
            this.lbl_project_name.BackColor = System.Drawing.Color.White;
            this.lbl_project_name.Dock = System.Windows.Forms.DockStyle.Left;
            this.lbl_project_name.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_project_name.Location = new System.Drawing.Point(0, 0);
            this.lbl_project_name.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_project_name.Name = "lbl_project_name";
            this.lbl_project_name.Size = new System.Drawing.Size(760, 40);
            this.lbl_project_name.TabIndex = 1;
            this.lbl_project_name.Text = "Project Name";
            this.lbl_project_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnl_left
            // 
            this.pnl_left.BackColor = System.Drawing.Color.FloralWhite;
            this.pnl_left.Controls.Add(this.btn_activity_log);
            this.pnl_left.Controls.Add(this.btn_task_add);
            this.pnl_left.Controls.Add(this.btn_tasks);
            this.pnl_left.Controls.Add(this.btn_worker_add);
            this.pnl_left.Controls.Add(this.btn_workers);
            this.pnl_left.Controls.Add(this.btn_link);
            this.pnl_left.Controls.Add(this.btn_settings);
            this.pnl_left.Controls.Add(this.btn_exit);
            this.pnl_left.Controls.Add(this.btn_logout);
            this.pnl_left.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnl_left.HorizontalScrollbarBarColor = true;
            this.pnl_left.HorizontalScrollbarHighlightOnWheel = false;
            this.pnl_left.HorizontalScrollbarSize = 10;
            this.pnl_left.Location = new System.Drawing.Point(20, 100);
            this.pnl_left.Margin = new System.Windows.Forms.Padding(0);
            this.pnl_left.Name = "pnl_left";
            this.pnl_left.Size = new System.Drawing.Size(50, 480);
            this.pnl_left.TabIndex = 2;
            this.pnl_left.VerticalScrollbarBarColor = true;
            this.pnl_left.VerticalScrollbarHighlightOnWheel = false;
            this.pnl_left.VerticalScrollbarSize = 10;
            // 
            // btn_activity_log
            // 
            this.btn_activity_log.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_activity_log.BackgroundImage")));
            this.btn_activity_log.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_activity_log.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_activity_log.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_activity_log.Location = new System.Drawing.Point(0, 30);
            this.btn_activity_log.Margin = new System.Windows.Forms.Padding(0);
            this.btn_activity_log.Name = "btn_activity_log";
            this.btn_activity_log.Size = new System.Drawing.Size(50, 50);
            this.btn_activity_log.TabIndex = 3;
            this.btn_activity_log.UseSelectable = true;
            this.btn_activity_log.Click += new System.EventHandler(this.Btn_Activity_Log_Click);
            // 
            // btn_task_add
            // 
            this.btn_task_add.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_task_add.BackgroundImage")));
            this.btn_task_add.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_task_add.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_task_add.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_task_add.Location = new System.Drawing.Point(0, 80);
            this.btn_task_add.Margin = new System.Windows.Forms.Padding(0);
            this.btn_task_add.Name = "btn_task_add";
            this.btn_task_add.Size = new System.Drawing.Size(50, 50);
            this.btn_task_add.TabIndex = 4;
            this.btn_task_add.UseSelectable = true;
            this.btn_task_add.Click += new System.EventHandler(this.Btn_Task_Add_Click);
            // 
            // btn_tasks
            // 
            this.btn_tasks.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_tasks.BackgroundImage")));
            this.btn_tasks.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_tasks.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_tasks.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_tasks.Location = new System.Drawing.Point(0, 130);
            this.btn_tasks.Margin = new System.Windows.Forms.Padding(0);
            this.btn_tasks.Name = "btn_tasks";
            this.btn_tasks.Size = new System.Drawing.Size(50, 50);
            this.btn_tasks.TabIndex = 5;
            this.btn_tasks.UseSelectable = true;
            this.btn_tasks.Click += new System.EventHandler(this.Btn_Tasks_Click);
            // 
            // btn_worker_add
            // 
            this.btn_worker_add.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_worker_add.BackgroundImage")));
            this.btn_worker_add.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_worker_add.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_worker_add.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_worker_add.Location = new System.Drawing.Point(0, 180);
            this.btn_worker_add.Margin = new System.Windows.Forms.Padding(0);
            this.btn_worker_add.Name = "btn_worker_add";
            this.btn_worker_add.Size = new System.Drawing.Size(50, 50);
            this.btn_worker_add.TabIndex = 6;
            this.btn_worker_add.UseSelectable = true;
            this.btn_worker_add.Click += new System.EventHandler(this.Btn_User_Add_Click);
            // 
            // btn_workers
            // 
            this.btn_workers.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_workers.BackgroundImage")));
            this.btn_workers.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_workers.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_workers.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_workers.Location = new System.Drawing.Point(0, 230);
            this.btn_workers.Margin = new System.Windows.Forms.Padding(0);
            this.btn_workers.Name = "btn_workers";
            this.btn_workers.Size = new System.Drawing.Size(50, 50);
            this.btn_workers.TabIndex = 7;
            this.btn_workers.UseSelectable = true;
            this.btn_workers.Click += new System.EventHandler(this.Btn_Users_Click);
            // 
            // btn_link
            // 
            this.btn_link.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_link.BackgroundImage")));
            this.btn_link.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_link.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_link.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_link.Location = new System.Drawing.Point(0, 280);
            this.btn_link.Margin = new System.Windows.Forms.Padding(0);
            this.btn_link.Name = "btn_link";
            this.btn_link.Size = new System.Drawing.Size(50, 50);
            this.btn_link.TabIndex = 8;
            this.btn_link.UseSelectable = true;
            this.btn_link.Click += new System.EventHandler(this.Btn_Link_Click);
            // 
            // btn_settings
            // 
            this.btn_settings.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_settings.BackgroundImage")));
            this.btn_settings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_settings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_settings.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_settings.Location = new System.Drawing.Point(0, 330);
            this.btn_settings.Margin = new System.Windows.Forms.Padding(0);
            this.btn_settings.Name = "btn_settings";
            this.btn_settings.Size = new System.Drawing.Size(50, 50);
            this.btn_settings.TabIndex = 9;
            this.btn_settings.UseSelectable = true;
            this.btn_settings.Click += new System.EventHandler(this.Btn_Settings_Click);
            // 
            // btn_exit
            // 
            this.btn_exit.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_exit.BackgroundImage")));
            this.btn_exit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_exit.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_exit.Location = new System.Drawing.Point(0, 380);
            this.btn_exit.Margin = new System.Windows.Forms.Padding(0);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(50, 50);
            this.btn_exit.TabIndex = 10;
            this.btn_exit.UseSelectable = true;
            this.btn_exit.Click += new System.EventHandler(this.Btn_Exit_Click);
            // 
            // btn_logout
            // 
            this.btn_logout.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_logout.BackgroundImage")));
            this.btn_logout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_logout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_logout.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btn_logout.Location = new System.Drawing.Point(0, 430);
            this.btn_logout.Margin = new System.Windows.Forms.Padding(0);
            this.btn_logout.Name = "btn_logout";
            this.btn_logout.Size = new System.Drawing.Size(50, 50);
            this.btn_logout.TabIndex = 11;
            this.btn_logout.UseSelectable = true;
            this.btn_logout.Click += new System.EventHandler(this.Btn_Logout_Click);
            // 
            // tab_control
            // 
            this.tab_control.Controls.Add(this.tab_activity_log);
            this.tab_control.Controls.Add(this.tab_tasks);
            this.tab_control.Controls.Add(this.tab_workers);
            this.tab_control.Cursor = System.Windows.Forms.Cursors.Default;
            this.tab_control.Dock = System.Windows.Forms.DockStyle.Top;
            this.tab_control.FontWeight = MetroFramework.MetroTabControlWeight.Regular;
            this.tab_control.HotTrack = true;
            this.tab_control.ItemSize = new System.Drawing.Size(234, 25);
            this.tab_control.Location = new System.Drawing.Point(70, 100);
            this.tab_control.Margin = new System.Windows.Forms.Padding(0);
            this.tab_control.Name = "tab_control";
            this.tab_control.SelectedIndex = 0;
            this.tab_control.Size = new System.Drawing.Size(710, 420);
            this.tab_control.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tab_control.Style = MetroFramework.MetroColorStyle.Purple;
            this.tab_control.TabIndex = 12;
            this.tab_control.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.tab_control.UseSelectable = true;
            // 
            // tab_activity_log
            // 
            this.tab_activity_log.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tab_activity_log.Controls.Add(this.grid_activity_log);
            this.tab_activity_log.HorizontalScrollbarBarColor = true;
            this.tab_activity_log.HorizontalScrollbarHighlightOnWheel = false;
            this.tab_activity_log.HorizontalScrollbarSize = 10;
            this.tab_activity_log.Location = new System.Drawing.Point(4, 29);
            this.tab_activity_log.Margin = new System.Windows.Forms.Padding(0);
            this.tab_activity_log.Name = "tab_activity_log";
            this.tab_activity_log.Size = new System.Drawing.Size(702, 387);
            this.tab_activity_log.TabIndex = 0;
            this.tab_activity_log.Text = "Activity Log";
            this.tab_activity_log.VerticalScrollbarBarColor = true;
            this.tab_activity_log.VerticalScrollbarHighlightOnWheel = false;
            this.tab_activity_log.VerticalScrollbarSize = 10;
            // 
            // grid_activity_log
            // 
            this.grid_activity_log.AllowUserToAddRows = false;
            this.grid_activity_log.AllowUserToDeleteRows = false;
            this.grid_activity_log.AllowUserToResizeRows = false;
            this.grid_activity_log.AutoGenerateColumns = false;
            this.grid_activity_log.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.grid_activity_log.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grid_activity_log.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grid_activity_log.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.DarkSlateBlue;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Sitka Banner", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.SlateBlue;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grid_activity_log.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.grid_activity_log.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid_activity_log.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.operationDataGridViewTextBoxColumn,
            this.messageDataGridViewTextBoxColumn,
            this.dateDataGridViewTextBoxColumn});
            this.grid_activity_log.Cursor = System.Windows.Forms.Cursors.Hand;
            this.grid_activity_log.DataSource = this.activityBindingSource;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Sitka Banner", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grid_activity_log.DefaultCellStyle = dataGridViewCellStyle11;
            this.grid_activity_log.EnableHeadersVisualStyles = false;
            this.grid_activity_log.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.grid_activity_log.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.grid_activity_log.Location = new System.Drawing.Point(3, 3);
            this.grid_activity_log.Margin = new System.Windows.Forms.Padding(0);
            this.grid_activity_log.Name = "grid_activity_log";
            this.grid_activity_log.ReadOnly = true;
            this.grid_activity_log.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grid_activity_log.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.grid_activity_log.RowHeadersVisible = false;
            this.grid_activity_log.RowHeadersWidth = 40;
            this.grid_activity_log.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grid_activity_log.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid_activity_log.Size = new System.Drawing.Size(694, 397);
            this.grid_activity_log.TabIndex = 17;
            // 
            // operationDataGridViewTextBoxColumn
            // 
            this.operationDataGridViewTextBoxColumn.DataPropertyName = "Operation";
            this.operationDataGridViewTextBoxColumn.HeaderText = "Operation";
            this.operationDataGridViewTextBoxColumn.Name = "operationDataGridViewTextBoxColumn";
            this.operationDataGridViewTextBoxColumn.ReadOnly = true;
            this.operationDataGridViewTextBoxColumn.Width = 150;
            // 
            // messageDataGridViewTextBoxColumn
            // 
            this.messageDataGridViewTextBoxColumn.DataPropertyName = "Message";
            this.messageDataGridViewTextBoxColumn.HeaderText = "Message";
            this.messageDataGridViewTextBoxColumn.Name = "messageDataGridViewTextBoxColumn";
            this.messageDataGridViewTextBoxColumn.ReadOnly = true;
            this.messageDataGridViewTextBoxColumn.Width = 444;
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "Date";
            this.dateDataGridViewTextBoxColumn.HeaderText = "Date";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            this.dateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // activityBindingSource
            // 
            this.activityBindingSource.DataMember = "Activity";
            this.activityBindingSource.DataSource = this.humseeDataSetActivity;
            // 
            // humseeDataSetActivity
            // 
            this.humseeDataSetActivity.DataSetName = "HumseeDataSetActivity";
            this.humseeDataSetActivity.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tab_tasks
            // 
            this.tab_tasks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tab_tasks.Controls.Add(this.grid_tasks);
            this.tab_tasks.HorizontalScrollbarBarColor = true;
            this.tab_tasks.HorizontalScrollbarHighlightOnWheel = false;
            this.tab_tasks.HorizontalScrollbarSize = 10;
            this.tab_tasks.Location = new System.Drawing.Point(4, 29);
            this.tab_tasks.Margin = new System.Windows.Forms.Padding(0);
            this.tab_tasks.Name = "tab_tasks";
            this.tab_tasks.Size = new System.Drawing.Size(702, 387);
            this.tab_tasks.TabIndex = 1;
            this.tab_tasks.Text = "Tasks";
            this.tab_tasks.VerticalScrollbarBarColor = true;
            this.tab_tasks.VerticalScrollbarHighlightOnWheel = false;
            this.tab_tasks.VerticalScrollbarSize = 10;
            // 
            // grid_tasks
            // 
            this.grid_tasks.AllowUserToAddRows = false;
            this.grid_tasks.AllowUserToDeleteRows = false;
            this.grid_tasks.AllowUserToResizeRows = false;
            this.grid_tasks.AutoGenerateColumns = false;
            this.grid_tasks.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.grid_tasks.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grid_tasks.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grid_tasks.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.DarkSlateBlue;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Sitka Banner", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.SlateBlue;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grid_tasks.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.grid_tasks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid_tasks.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.workerDataGridViewTextBoxColumn,
            this.taskDataGridViewTextBoxColumn,
            this.taskStatusDataGridViewTextBoxColumn,
            this.startDateDataGridViewTextBoxColumn,
            this.endDateDataGridViewTextBoxColumn});
            this.grid_tasks.Cursor = System.Windows.Forms.Cursors.Hand;
            this.grid_tasks.DataSource = this.tasksBindingSource;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Sitka Banner", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grid_tasks.DefaultCellStyle = dataGridViewCellStyle14;
            this.grid_tasks.EnableHeadersVisualStyles = false;
            this.grid_tasks.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.grid_tasks.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.grid_tasks.Location = new System.Drawing.Point(3, 3);
            this.grid_tasks.Margin = new System.Windows.Forms.Padding(0);
            this.grid_tasks.Name = "grid_tasks";
            this.grid_tasks.ReadOnly = true;
            this.grid_tasks.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grid_tasks.RowHeadersDefaultCellStyle = dataGridViewCellStyle15;
            this.grid_tasks.RowHeadersVisible = false;
            this.grid_tasks.RowHeadersWidth = 40;
            this.grid_tasks.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grid_tasks.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid_tasks.Size = new System.Drawing.Size(694, 397);
            this.grid_tasks.TabIndex = 16;
            // 
            // workerDataGridViewTextBoxColumn
            // 
            this.workerDataGridViewTextBoxColumn.DataPropertyName = "Worker";
            this.workerDataGridViewTextBoxColumn.HeaderText = "Worker";
            this.workerDataGridViewTextBoxColumn.Name = "workerDataGridViewTextBoxColumn";
            this.workerDataGridViewTextBoxColumn.ReadOnly = true;
            this.workerDataGridViewTextBoxColumn.Width = 150;
            // 
            // taskDataGridViewTextBoxColumn
            // 
            this.taskDataGridViewTextBoxColumn.DataPropertyName = "Task";
            this.taskDataGridViewTextBoxColumn.HeaderText = "Task";
            this.taskDataGridViewTextBoxColumn.Name = "taskDataGridViewTextBoxColumn";
            this.taskDataGridViewTextBoxColumn.ReadOnly = true;
            this.taskDataGridViewTextBoxColumn.Width = 219;
            // 
            // taskStatusDataGridViewTextBoxColumn
            // 
            this.taskStatusDataGridViewTextBoxColumn.DataPropertyName = "Task Status";
            this.taskStatusDataGridViewTextBoxColumn.HeaderText = "Task Status";
            this.taskStatusDataGridViewTextBoxColumn.Name = "taskStatusDataGridViewTextBoxColumn";
            this.taskStatusDataGridViewTextBoxColumn.ReadOnly = true;
            this.taskStatusDataGridViewTextBoxColumn.Width = 125;
            // 
            // startDateDataGridViewTextBoxColumn
            // 
            this.startDateDataGridViewTextBoxColumn.DataPropertyName = "Start Date";
            this.startDateDataGridViewTextBoxColumn.HeaderText = "Start Date";
            this.startDateDataGridViewTextBoxColumn.Name = "startDateDataGridViewTextBoxColumn";
            this.startDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // endDateDataGridViewTextBoxColumn
            // 
            this.endDateDataGridViewTextBoxColumn.DataPropertyName = "End Date";
            this.endDateDataGridViewTextBoxColumn.HeaderText = "End Date";
            this.endDateDataGridViewTextBoxColumn.Name = "endDateDataGridViewTextBoxColumn";
            this.endDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // tasksBindingSource
            // 
            this.tasksBindingSource.DataMember = "Tasks";
            this.tasksBindingSource.DataSource = this.humseeDataSetTasks;
            // 
            // humseeDataSetTasks
            // 
            this.humseeDataSetTasks.DataSetName = "HumseeDataSetTasks";
            this.humseeDataSetTasks.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tab_workers
            // 
            this.tab_workers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tab_workers.Controls.Add(this.grid_workers);
            this.tab_workers.HorizontalScrollbarBarColor = true;
            this.tab_workers.HorizontalScrollbarHighlightOnWheel = false;
            this.tab_workers.HorizontalScrollbarSize = 10;
            this.tab_workers.Location = new System.Drawing.Point(4, 29);
            this.tab_workers.Margin = new System.Windows.Forms.Padding(0);
            this.tab_workers.Name = "tab_workers";
            this.tab_workers.Size = new System.Drawing.Size(702, 387);
            this.tab_workers.TabIndex = 2;
            this.tab_workers.Text = "Workers";
            this.tab_workers.VerticalScrollbarBarColor = true;
            this.tab_workers.VerticalScrollbarHighlightOnWheel = false;
            this.tab_workers.VerticalScrollbarSize = 10;
            // 
            // grid_workers
            // 
            this.grid_workers.AllowUserToAddRows = false;
            this.grid_workers.AllowUserToDeleteRows = false;
            this.grid_workers.AllowUserToResizeRows = false;
            this.grid_workers.AutoGenerateColumns = false;
            this.grid_workers.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.grid_workers.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grid_workers.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grid_workers.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.DarkSlateBlue;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Sitka Banner", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.SlateBlue;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grid_workers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.grid_workers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid_workers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.firstNameDataGridViewTextBoxColumn,
            this.lastNameDataGridViewTextBoxColumn,
            this.positionDataGridViewTextBoxColumn,
            this.emailDataGridViewTextBoxColumn,
            this.phoneNumberDataGridViewTextBoxColumn});
            this.grid_workers.Cursor = System.Windows.Forms.Cursors.Hand;
            this.grid_workers.DataSource = this.workersBindingSource;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Sitka Banner", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle17.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grid_workers.DefaultCellStyle = dataGridViewCellStyle17;
            this.grid_workers.EnableHeadersVisualStyles = false;
            this.grid_workers.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.grid_workers.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.grid_workers.Location = new System.Drawing.Point(3, 3);
            this.grid_workers.Margin = new System.Windows.Forms.Padding(0);
            this.grid_workers.Name = "grid_workers";
            this.grid_workers.ReadOnly = true;
            this.grid_workers.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grid_workers.RowHeadersDefaultCellStyle = dataGridViewCellStyle18;
            this.grid_workers.RowHeadersVisible = false;
            this.grid_workers.RowHeadersWidth = 40;
            this.grid_workers.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.grid_workers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grid_workers.Size = new System.Drawing.Size(694, 397);
            this.grid_workers.TabIndex = 15;
            // 
            // firstNameDataGridViewTextBoxColumn
            // 
            this.firstNameDataGridViewTextBoxColumn.DataPropertyName = "First Name";
            this.firstNameDataGridViewTextBoxColumn.HeaderText = "First Name";
            this.firstNameDataGridViewTextBoxColumn.Name = "firstNameDataGridViewTextBoxColumn";
            this.firstNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.firstNameDataGridViewTextBoxColumn.Width = 150;
            // 
            // lastNameDataGridViewTextBoxColumn
            // 
            this.lastNameDataGridViewTextBoxColumn.DataPropertyName = "Last Name";
            this.lastNameDataGridViewTextBoxColumn.HeaderText = "Last Name";
            this.lastNameDataGridViewTextBoxColumn.Name = "lastNameDataGridViewTextBoxColumn";
            this.lastNameDataGridViewTextBoxColumn.ReadOnly = true;
            this.lastNameDataGridViewTextBoxColumn.Width = 150;
            // 
            // positionDataGridViewTextBoxColumn
            // 
            this.positionDataGridViewTextBoxColumn.DataPropertyName = "Position";
            this.positionDataGridViewTextBoxColumn.HeaderText = "Position";
            this.positionDataGridViewTextBoxColumn.Name = "positionDataGridViewTextBoxColumn";
            this.positionDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // emailDataGridViewTextBoxColumn
            // 
            this.emailDataGridViewTextBoxColumn.DataPropertyName = "Email";
            this.emailDataGridViewTextBoxColumn.HeaderText = "Email";
            this.emailDataGridViewTextBoxColumn.Name = "emailDataGridViewTextBoxColumn";
            this.emailDataGridViewTextBoxColumn.ReadOnly = true;
            this.emailDataGridViewTextBoxColumn.Width = 175;
            // 
            // phoneNumberDataGridViewTextBoxColumn
            // 
            this.phoneNumberDataGridViewTextBoxColumn.DataPropertyName = "Phone Number";
            this.phoneNumberDataGridViewTextBoxColumn.HeaderText = "Phone Number";
            this.phoneNumberDataGridViewTextBoxColumn.Name = "phoneNumberDataGridViewTextBoxColumn";
            this.phoneNumberDataGridViewTextBoxColumn.ReadOnly = true;
            this.phoneNumberDataGridViewTextBoxColumn.Width = 125;
            // 
            // workersBindingSource
            // 
            this.workersBindingSource.DataMember = "Workers";
            this.workersBindingSource.DataSource = this.humseeDataSetWorkersBindingSource;
            // 
            // humseeDataSetWorkersBindingSource
            // 
            this.humseeDataSetWorkersBindingSource.DataSource = this.humseeDataSetWorkers;
            this.humseeDataSetWorkersBindingSource.Position = 0;
            // 
            // humseeDataSetWorkers
            // 
            this.humseeDataSetWorkers.DataSetName = "HumseeDataSetWorkers";
            this.humseeDataSetWorkers.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pnl_down
            // 
            this.pnl_down.Controls.Add(this.lbl_select_worker);
            this.pnl_down.Controls.Add(this.cb_select_worker);
            this.pnl_down.Controls.Add(this.btn_remove_worker);
            this.pnl_down.Controls.Add(this.lbl_select_task);
            this.pnl_down.Controls.Add(this.cb_select_task);
            this.pnl_down.Controls.Add(this.btn_done_task);
            this.pnl_down.Controls.Add(this.btn_remove_task);
            this.pnl_down.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnl_down.HorizontalScrollbarBarColor = true;
            this.pnl_down.HorizontalScrollbarHighlightOnWheel = false;
            this.pnl_down.HorizontalScrollbarSize = 10;
            this.pnl_down.Location = new System.Drawing.Point(70, 520);
            this.pnl_down.Margin = new System.Windows.Forms.Padding(0);
            this.pnl_down.Name = "pnl_down";
            this.pnl_down.Size = new System.Drawing.Size(710, 60);
            this.pnl_down.TabIndex = 13;
            this.pnl_down.VerticalScrollbarBarColor = true;
            this.pnl_down.VerticalScrollbarHighlightOnWheel = false;
            this.pnl_down.VerticalScrollbarSize = 10;
            // 
            // lbl_select_worker
            // 
            this.lbl_select_worker.Enabled = false;
            this.lbl_select_worker.Location = new System.Drawing.Point(14, 2);
            this.lbl_select_worker.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_select_worker.Name = "lbl_select_worker";
            this.lbl_select_worker.Size = new System.Drawing.Size(260, 25);
            this.lbl_select_worker.TabIndex = 13;
            this.lbl_select_worker.Text = "Select Worker";
            this.lbl_select_worker.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_select_worker.UseMnemonic = false;
            // 
            // cb_select_worker
            // 
            this.cb_select_worker.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb_select_worker.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.workersBindingSource, "Email", true));
            this.cb_select_worker.DataSource = this.workersBindingSource;
            this.cb_select_worker.DisplayMember = "Email";
            this.cb_select_worker.DropDownHeight = 100;
            this.cb_select_worker.Enabled = false;
            this.cb_select_worker.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cb_select_worker.FormattingEnabled = true;
            this.cb_select_worker.IntegralHeight = false;
            this.cb_select_worker.ItemHeight = 19;
            this.cb_select_worker.Location = new System.Drawing.Point(14, 27);
            this.cb_select_worker.Margin = new System.Windows.Forms.Padding(0);
            this.cb_select_worker.MaxDropDownItems = 5;
            this.cb_select_worker.Name = "cb_select_worker";
            this.cb_select_worker.Size = new System.Drawing.Size(260, 25);
            this.cb_select_worker.TabIndex = 12;
            this.cb_select_worker.UseSelectable = true;
            this.cb_select_worker.ValueMember = "Email";
            // 
            // btn_remove_worker
            // 
            this.btn_remove_worker.BackColor = System.Drawing.Color.FloralWhite;
            this.btn_remove_worker.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_remove_worker.BackgroundImage")));
            this.btn_remove_worker.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_remove_worker.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_remove_worker.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_remove_worker.ForeColor = System.Drawing.Color.White;
            this.btn_remove_worker.Location = new System.Drawing.Point(284, 23);
            this.btn_remove_worker.Margin = new System.Windows.Forms.Padding(10);
            this.btn_remove_worker.Name = "btn_remove_worker";
            this.btn_remove_worker.Size = new System.Drawing.Size(32, 32);
            this.btn_remove_worker.TabIndex = 11;
            this.btn_remove_worker.UseCustomBackColor = true;
            this.btn_remove_worker.UseCustomForeColor = true;
            this.btn_remove_worker.UseMnemonic = false;
            this.btn_remove_worker.UseSelectable = true;
            this.btn_remove_worker.Click += new System.EventHandler(this.Btn_Remove_Worker_Click);
            // 
            // lbl_select_task
            // 
            this.lbl_select_task.Enabled = false;
            this.lbl_select_task.Location = new System.Drawing.Point(350, 2);
            this.lbl_select_task.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_select_task.Name = "lbl_select_task";
            this.lbl_select_task.Size = new System.Drawing.Size(260, 25);
            this.lbl_select_task.TabIndex = 10;
            this.lbl_select_task.Text = "Select Task";
            this.lbl_select_task.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_select_task.UseMnemonic = false;
            // 
            // cb_select_task
            // 
            this.cb_select_task.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb_select_task.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.tasksBindingSource, "Task", true));
            this.cb_select_task.DataSource = this.tasksBindingSource;
            this.cb_select_task.DisplayMember = "Task";
            this.cb_select_task.DropDownHeight = 100;
            this.cb_select_task.Enabled = false;
            this.cb_select_task.FontSize = MetroFramework.MetroComboBoxSize.Small;
            this.cb_select_task.FormattingEnabled = true;
            this.cb_select_task.IntegralHeight = false;
            this.cb_select_task.ItemHeight = 19;
            this.cb_select_task.Location = new System.Drawing.Point(350, 27);
            this.cb_select_task.Margin = new System.Windows.Forms.Padding(0);
            this.cb_select_task.MaxDropDownItems = 5;
            this.cb_select_task.Name = "cb_select_task";
            this.cb_select_task.Size = new System.Drawing.Size(260, 25);
            this.cb_select_task.TabIndex = 9;
            this.cb_select_task.UseSelectable = true;
            this.cb_select_task.ValueMember = "Task";
            // 
            // btn_done_task
            // 
            this.btn_done_task.BackColor = System.Drawing.Color.FloralWhite;
            this.btn_done_task.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_done_task.BackgroundImage")));
            this.btn_done_task.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_done_task.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_done_task.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_done_task.ForeColor = System.Drawing.Color.White;
            this.btn_done_task.Location = new System.Drawing.Point(663, 23);
            this.btn_done_task.Margin = new System.Windows.Forms.Padding(10);
            this.btn_done_task.Name = "btn_done_task";
            this.btn_done_task.Size = new System.Drawing.Size(32, 32);
            this.btn_done_task.TabIndex = 8;
            this.btn_done_task.UseCustomBackColor = true;
            this.btn_done_task.UseCustomForeColor = true;
            this.btn_done_task.UseMnemonic = false;
            this.btn_done_task.UseSelectable = true;
            this.btn_done_task.Click += new System.EventHandler(this.Btn_Done_Task_Click);
            // 
            // btn_remove_task
            // 
            this.btn_remove_task.BackColor = System.Drawing.Color.FloralWhite;
            this.btn_remove_task.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_remove_task.BackgroundImage")));
            this.btn_remove_task.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_remove_task.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_remove_task.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_remove_task.ForeColor = System.Drawing.Color.White;
            this.btn_remove_task.Location = new System.Drawing.Point(620, 23);
            this.btn_remove_task.Margin = new System.Windows.Forms.Padding(10);
            this.btn_remove_task.Name = "btn_remove_task";
            this.btn_remove_task.Size = new System.Drawing.Size(32, 32);
            this.btn_remove_task.TabIndex = 7;
            this.btn_remove_task.UseCustomBackColor = true;
            this.btn_remove_task.UseCustomForeColor = true;
            this.btn_remove_task.UseMnemonic = false;
            this.btn_remove_task.UseSelectable = true;
            this.btn_remove_task.Click += new System.EventHandler(this.Btn_Remove_Task_Click);
            // 
            // tasksTableAdapter
            // 
            this.tasksTableAdapter.ClearBeforeFill = true;
            // 
            // workersTableAdapter
            // 
            this.workersTableAdapter.ClearBeforeFill = true;
            // 
            // activityTableAdapter
            // 
            this.activityTableAdapter.ClearBeforeFill = true;
            // 
            // Home
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.ControlBox = false;
            this.Controls.Add(this.pnl_down);
            this.Controls.Add(this.tab_control);
            this.Controls.Add(this.pnl_left);
            this.Controls.Add(this.pnl_top);
            this.Font = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Home";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.Style = MetroFramework.MetroColorStyle.Purple;
            this.Text = "Humsee Home";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Activated += new System.EventHandler(this.Home_Activated);
            this.Load += new System.EventHandler(this.Home_Load);
            this.pnl_top.ResumeLayout(false);
            this.pnl_left.ResumeLayout(false);
            this.tab_control.ResumeLayout(false);
            this.tab_activity_log.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid_activity_log)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.activityBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.humseeDataSetActivity)).EndInit();
            this.tab_tasks.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid_tasks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tasksBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.humseeDataSetTasks)).EndInit();
            this.tab_workers.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grid_workers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.workersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.humseeDataSetWorkersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.humseeDataSetWorkers)).EndInit();
            this.pnl_down.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private MetroFramework.Controls.MetroButton btn_logout;
        private MetroFramework.Controls.MetroButton btn_exit;
        private MetroFramework.Controls.MetroButton btn_settings;
        private MetroFramework.Controls.MetroButton btn_link;
        private MetroFramework.Controls.MetroButton btn_worker_add;
        private MetroFramework.Controls.MetroButton btn_workers;
        private MetroFramework.Controls.MetroButton btn_tasks;
        private MetroFramework.Controls.MetroButton btn_task_add;
        private MetroFramework.Controls.MetroButton btn_activity_log;
        private MetroFramework.Controls.MetroPanel pnl_top;
        private MetroFramework.Controls.MetroLabel lbl_project_name;
        private MetroFramework.Controls.MetroPanel pnl_left;
        private MetroFramework.Controls.MetroTabControl tab_control;
        private MetroFramework.Controls.MetroTabPage tab_activity_log;
        private MetroFramework.Controls.MetroTabPage tab_tasks;
        private MetroFramework.Controls.MetroTabPage tab_workers;
        private MetroFramework.Controls.MetroGrid grid_workers;
        private MetroFramework.Controls.MetroPanel pnl_down;
        private MetroFramework.Controls.MetroButton btn_remove_task;
        private MetroFramework.Controls.MetroGrid grid_tasks;
        private HumseeDataSetTasks humseeDataSetTasks;
        private System.Windows.Forms.BindingSource tasksBindingSource;
        private HumseeDataSetTasksTableAdapters.TasksTableAdapter tasksTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn workerDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn taskStatusDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn startDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn endDateDataGridViewTextBoxColumn;
        private MetroFramework.Controls.MetroButton btn_done_task;
        private MetroFramework.Controls.MetroComboBox cb_select_task;
        private MetroFramework.Controls.MetroLabel lbl_select_task;
        private MetroFramework.Controls.MetroLabel lbl_select_worker;
        private MetroFramework.Controls.MetroComboBox cb_select_worker;
        private MetroFramework.Controls.MetroButton btn_remove_worker;
        private System.Windows.Forms.BindingSource humseeDataSetWorkersBindingSource;
        private HumseeDataSetWorkers humseeDataSetWorkers;
        private System.Windows.Forms.BindingSource workersBindingSource;
        private HumseeDataSetWorkersTableAdapters.WorkersTableAdapter workersTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn positionDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn emailDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn phoneNumberDataGridViewTextBoxColumn;
        private MetroFramework.Controls.MetroGrid grid_activity_log;
        private HumseeDataSetActivity humseeDataSetActivity;
        private System.Windows.Forms.BindingSource activityBindingSource;
        private HumseeDataSetActivityTableAdapters.ActivityTableAdapter activityTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn operationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn messageDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
    }
}
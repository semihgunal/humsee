﻿using System;
using MetroFramework.Forms;

namespace Humsee
{
    public partial class Login : MetroForm
    {
        public Login()
        {
            InitializeComponent();
        }

        private void Btn_Login_Click(object sender, EventArgs e)
        {
            if (MemberUnit.Login(tb_user_name.Text, tb_password.Text))
            {
                // Member Name Transfer
                ProjectUnit.UserName = tb_user_name.Text;
                // Login Form Hide
                Login.ActiveForm.Hide();
                // Project Form Open
                Project Form = new Project();
                Form.Show();
            }
        }

        private void Btn_Create_Account_Click(object sender, EventArgs e)
        {
            Register Form = new Register();
            Form.ShowDialog();
        }
    }
}

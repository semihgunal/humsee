﻿using System;
using MetroFramework.Forms;

namespace Humsee
{
    public partial class New_Task : MetroForm
    {
        public New_Task()
        {
            InitializeComponent();
        }

        private void New_Task_Load(object sender, EventArgs e)
        {
            // Data Base Fill
            this.workersTableAdapter.Fill(this.humseeDataSetWorkers.Workers, DataBaseUnit.ProjectTable[0]);
            // Min Date Set
            dt_end.MinDate = DateTime.Now;
            dt_end.MaxDate = DateTime.ParseExact(DataBaseUnit.ProjectTable[4], "dd.MM.yyyy", System.Globalization.CultureInfo.InvariantCulture);
        }

        private void Btn_Create_Click(object sender, EventArgs e)
        {
            HomeUnit.AddTask(cb_select_worker.Text, tb_task.Text, "Continue".ToString(), dt_end.Text);
        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            New_Task.ActiveForm.Dispose();
        }
    }
}

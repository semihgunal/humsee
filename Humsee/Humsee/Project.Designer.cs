﻿namespace Humsee
{
    partial class Project
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Project));
            this.lbl_select_project = new MetroFramework.Controls.MetroLabel();
            this.lbl_space01 = new MetroFramework.Controls.MetroLabel();
            this.lbl_space02 = new MetroFramework.Controls.MetroLabel();
            this.lbl_space03 = new MetroFramework.Controls.MetroLabel();
            this.cb_select_project = new MetroFramework.Controls.MetroComboBox();
            this.btn_open = new MetroFramework.Controls.MetroButton();
            this.btn_new_project = new MetroFramework.Controls.MetroButton();
            this.btn_logout = new MetroFramework.Controls.MetroButton();
            this.humseeDataSetProjects = new Humsee.HumseeDataSetProjects();
            this.humseeDataSetProjectsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.projectsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.projectsTableAdapter = new Humsee.HumseeDataSetProjectsTableAdapters.ProjectsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.humseeDataSetProjects)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.humseeDataSetProjectsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_select_project
            // 
            this.lbl_select_project.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_select_project.Enabled = false;
            this.lbl_select_project.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_select_project.Location = new System.Drawing.Point(20, 60);
            this.lbl_select_project.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_select_project.Name = "lbl_select_project";
            this.lbl_select_project.Size = new System.Drawing.Size(260, 25);
            this.lbl_select_project.TabIndex = 0;
            this.lbl_select_project.Text = "Select Project";
            this.lbl_select_project.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_select_project.UseMnemonic = false;
            // 
            // lbl_space01
            // 
            this.lbl_space01.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_space01.Enabled = false;
            this.lbl_space01.Location = new System.Drawing.Point(20, 114);
            this.lbl_space01.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_space01.Name = "lbl_space01";
            this.lbl_space01.Size = new System.Drawing.Size(260, 11);
            this.lbl_space01.TabIndex = 2;
            this.lbl_space01.UseMnemonic = false;
            // 
            // lbl_space02
            // 
            this.lbl_space02.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_space02.Enabled = false;
            this.lbl_space02.Location = new System.Drawing.Point(20, 160);
            this.lbl_space02.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_space02.Name = "lbl_space02";
            this.lbl_space02.Size = new System.Drawing.Size(260, 5);
            this.lbl_space02.TabIndex = 4;
            this.lbl_space02.UseMnemonic = false;
            // 
            // lbl_space03
            // 
            this.lbl_space03.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_space03.Enabled = false;
            this.lbl_space03.Location = new System.Drawing.Point(20, 200);
            this.lbl_space03.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_space03.Name = "lbl_space03";
            this.lbl_space03.Size = new System.Drawing.Size(260, 20);
            this.lbl_space03.TabIndex = 6;
            this.lbl_space03.UseMnemonic = false;
            // 
            // cb_select_project
            // 
            this.cb_select_project.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cb_select_project.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.projectsBindingSource, "Name", true));
            this.cb_select_project.DataSource = this.projectsBindingSource;
            this.cb_select_project.DisplayMember = "Name";
            this.cb_select_project.Dock = System.Windows.Forms.DockStyle.Top;
            this.cb_select_project.DropDownHeight = 125;
            this.cb_select_project.FormattingEnabled = true;
            this.cb_select_project.IntegralHeight = false;
            this.cb_select_project.ItemHeight = 23;
            this.cb_select_project.Location = new System.Drawing.Point(20, 85);
            this.cb_select_project.Margin = new System.Windows.Forms.Padding(0);
            this.cb_select_project.MaxDropDownItems = 5;
            this.cb_select_project.Name = "cb_select_project";
            this.cb_select_project.Size = new System.Drawing.Size(260, 29);
            this.cb_select_project.TabIndex = 1;
            this.cb_select_project.UseSelectable = true;
            this.cb_select_project.ValueMember = "Name";
            // 
            // btn_open
            // 
            this.btn_open.BackColor = System.Drawing.Color.HotPink;
            this.btn_open.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_open.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_open.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_open.ForeColor = System.Drawing.Color.White;
            this.btn_open.Location = new System.Drawing.Point(20, 125);
            this.btn_open.Margin = new System.Windows.Forms.Padding(0);
            this.btn_open.Name = "btn_open";
            this.btn_open.Size = new System.Drawing.Size(260, 35);
            this.btn_open.TabIndex = 3;
            this.btn_open.Text = "Open";
            this.btn_open.UseCustomBackColor = true;
            this.btn_open.UseCustomForeColor = true;
            this.btn_open.UseMnemonic = false;
            this.btn_open.UseSelectable = true;
            this.btn_open.Click += new System.EventHandler(this.Btn_Open_Click);
            // 
            // btn_new_project
            // 
            this.btn_new_project.BackColor = System.Drawing.Color.DeepPink;
            this.btn_new_project.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_new_project.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_new_project.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_new_project.ForeColor = System.Drawing.Color.White;
            this.btn_new_project.Location = new System.Drawing.Point(20, 165);
            this.btn_new_project.Margin = new System.Windows.Forms.Padding(0);
            this.btn_new_project.Name = "btn_new_project";
            this.btn_new_project.Size = new System.Drawing.Size(260, 35);
            this.btn_new_project.TabIndex = 5;
            this.btn_new_project.Text = "New Project";
            this.btn_new_project.UseCustomBackColor = true;
            this.btn_new_project.UseCustomForeColor = true;
            this.btn_new_project.UseMnemonic = false;
            this.btn_new_project.UseSelectable = true;
            this.btn_new_project.Click += new System.EventHandler(this.Btn_New_Project_Click);
            // 
            // btn_logout
            // 
            this.btn_logout.BackColor = System.Drawing.Color.Crimson;
            this.btn_logout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_logout.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_logout.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_logout.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_logout.ForeColor = System.Drawing.Color.White;
            this.btn_logout.Location = new System.Drawing.Point(20, 220);
            this.btn_logout.Margin = new System.Windows.Forms.Padding(0);
            this.btn_logout.Name = "btn_logout";
            this.btn_logout.Size = new System.Drawing.Size(260, 35);
            this.btn_logout.TabIndex = 7;
            this.btn_logout.Text = "Logout";
            this.btn_logout.UseCustomBackColor = true;
            this.btn_logout.UseCustomForeColor = true;
            this.btn_logout.UseMnemonic = false;
            this.btn_logout.UseSelectable = true;
            this.btn_logout.Click += new System.EventHandler(this.Btn_Logout_Click);
            // 
            // humseeDataSetProjects
            // 
            this.humseeDataSetProjects.DataSetName = "HumseeDataSetProjects";
            this.humseeDataSetProjects.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // humseeDataSetProjectsBindingSource
            // 
            this.humseeDataSetProjectsBindingSource.DataSource = this.humseeDataSetProjects;
            this.humseeDataSetProjectsBindingSource.Position = 0;
            // 
            // projectsBindingSource
            // 
            this.projectsBindingSource.DataMember = "Projects";
            this.projectsBindingSource.DataSource = this.humseeDataSetProjectsBindingSource;
            // 
            // projectsTableAdapter
            // 
            this.projectsTableAdapter.ClearBeforeFill = true;
            // 
            // Project
            // 
            this.AcceptButton = this.btn_open;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton = this.btn_logout;
            this.ClientSize = new System.Drawing.Size(300, 275);
            this.ControlBox = false;
            this.Controls.Add(this.btn_logout);
            this.Controls.Add(this.lbl_space03);
            this.Controls.Add(this.btn_new_project);
            this.Controls.Add(this.lbl_space02);
            this.Controls.Add(this.btn_open);
            this.Controls.Add(this.lbl_space01);
            this.Controls.Add(this.cb_select_project);
            this.Controls.Add(this.lbl_select_project);
            this.Font = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Project";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.Style = MetroFramework.MetroColorStyle.Pink;
            this.Text = "Project";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Activated += new System.EventHandler(this.Project_Activated);
            ((System.ComponentModel.ISupportInitialize)(this.humseeDataSetProjects)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.humseeDataSetProjectsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroComboBox cb_select_project;
        private MetroFramework.Controls.MetroButton btn_open;
        private MetroFramework.Controls.MetroButton btn_new_project;
        private MetroFramework.Controls.MetroButton btn_logout;
        private MetroFramework.Controls.MetroLabel lbl_select_project;
        private MetroFramework.Controls.MetroLabel lbl_space01;
        private MetroFramework.Controls.MetroLabel lbl_space02;
        private MetroFramework.Controls.MetroLabel lbl_space03;
        private System.Windows.Forms.BindingSource humseeDataSetProjectsBindingSource;
        private HumseeDataSetProjects humseeDataSetProjects;
        private System.Windows.Forms.BindingSource projectsBindingSource;
        private HumseeDataSetProjectsTableAdapters.ProjectsTableAdapter projectsTableAdapter;
    }
}
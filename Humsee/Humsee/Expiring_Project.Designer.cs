﻿namespace Humsee
{
    partial class Expiring_Project
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.tb_project_name = new MetroFramework.Controls.MetroTextBox();
            this.lbl_project_name = new MetroFramework.Controls.MetroLabel();
            this.btn_ignore = new MetroFramework.Controls.MetroButton();
            this.lbl_space02 = new MetroFramework.Controls.MetroLabel();
            this.btn_extend_date = new MetroFramework.Controls.MetroButton();
            this.lbl_space01 = new MetroFramework.Controls.MetroLabel();
            this.dt_new_end = new MetroFramework.Controls.MetroDateTime();
            this.lbl_new_end_date = new MetroFramework.Controls.MetroLabel();
            this.SuspendLayout();
            // 
            // tb_project_name
            // 
            // 
            // 
            // 
            this.tb_project_name.CustomButton.Image = null;
            this.tb_project_name.CustomButton.Location = new System.Drawing.Point(232, 2);
            this.tb_project_name.CustomButton.Name = "";
            this.tb_project_name.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tb_project_name.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_project_name.CustomButton.TabIndex = 1;
            this.tb_project_name.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_project_name.CustomButton.UseSelectable = true;
            this.tb_project_name.CustomButton.Visible = false;
            this.tb_project_name.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_project_name.Enabled = false;
            this.tb_project_name.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_project_name.Lines = new string[0];
            this.tb_project_name.Location = new System.Drawing.Point(20, 85);
            this.tb_project_name.Margin = new System.Windows.Forms.Padding(0);
            this.tb_project_name.MaxLength = 30;
            this.tb_project_name.Name = "tb_project_name";
            this.tb_project_name.PasswordChar = '\0';
            this.tb_project_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_project_name.SelectedText = "";
            this.tb_project_name.SelectionLength = 0;
            this.tb_project_name.SelectionStart = 0;
            this.tb_project_name.ShortcutsEnabled = true;
            this.tb_project_name.Size = new System.Drawing.Size(260, 30);
            this.tb_project_name.TabIndex = 1;
            this.tb_project_name.UseSelectable = true;
            this.tb_project_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_project_name.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            // 
            // lbl_project_name
            // 
            this.lbl_project_name.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_project_name.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_project_name.Location = new System.Drawing.Point(20, 60);
            this.lbl_project_name.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_project_name.Name = "lbl_project_name";
            this.lbl_project_name.Size = new System.Drawing.Size(260, 25);
            this.lbl_project_name.TabIndex = 0;
            this.lbl_project_name.Text = "Project Name";
            this.lbl_project_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_project_name.UseMnemonic = false;
            // 
            // btn_ignore
            // 
            this.btn_ignore.BackColor = System.Drawing.Color.Orange;
            this.btn_ignore.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_ignore.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_ignore.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_ignore.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_ignore.ForeColor = System.Drawing.Color.White;
            this.btn_ignore.Location = new System.Drawing.Point(20, 219);
            this.btn_ignore.Margin = new System.Windows.Forms.Padding(0);
            this.btn_ignore.Name = "btn_ignore";
            this.btn_ignore.Size = new System.Drawing.Size(260, 35);
            this.btn_ignore.TabIndex = 7;
            this.btn_ignore.Text = "Ignore";
            this.btn_ignore.UseCustomBackColor = true;
            this.btn_ignore.UseCustomForeColor = true;
            this.btn_ignore.UseMnemonic = false;
            this.btn_ignore.UseSelectable = true;
            this.btn_ignore.Click += new System.EventHandler(this.Btn_Ignore_Click);
            // 
            // lbl_space02
            // 
            this.lbl_space02.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_space02.Enabled = false;
            this.lbl_space02.Location = new System.Drawing.Point(20, 214);
            this.lbl_space02.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_space02.Name = "lbl_space02";
            this.lbl_space02.Size = new System.Drawing.Size(260, 5);
            this.lbl_space02.TabIndex = 6;
            this.lbl_space02.UseMnemonic = false;
            // 
            // btn_extend_date
            // 
            this.btn_extend_date.BackColor = System.Drawing.Color.OliveDrab;
            this.btn_extend_date.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_extend_date.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_extend_date.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_extend_date.ForeColor = System.Drawing.Color.White;
            this.btn_extend_date.Location = new System.Drawing.Point(20, 179);
            this.btn_extend_date.Margin = new System.Windows.Forms.Padding(0);
            this.btn_extend_date.Name = "btn_extend_date";
            this.btn_extend_date.Size = new System.Drawing.Size(260, 35);
            this.btn_extend_date.TabIndex = 5;
            this.btn_extend_date.Text = "Extend the Date";
            this.btn_extend_date.UseCustomBackColor = true;
            this.btn_extend_date.UseCustomForeColor = true;
            this.btn_extend_date.UseMnemonic = false;
            this.btn_extend_date.UseSelectable = true;
            this.btn_extend_date.Click += new System.EventHandler(this.Btn_Extend_Date_Click);
            // 
            // lbl_space01
            // 
            this.lbl_space01.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_space01.Enabled = false;
            this.lbl_space01.Location = new System.Drawing.Point(20, 169);
            this.lbl_space01.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_space01.Name = "lbl_space01";
            this.lbl_space01.Size = new System.Drawing.Size(260, 10);
            this.lbl_space01.TabIndex = 4;
            this.lbl_space01.UseMnemonic = false;
            // 
            // dt_new_end
            // 
            this.dt_new_end.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dt_new_end.CustomFormat = "dd.MM.yyyy";
            this.dt_new_end.Dock = System.Windows.Forms.DockStyle.Top;
            this.dt_new_end.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dt_new_end.Location = new System.Drawing.Point(20, 140);
            this.dt_new_end.Margin = new System.Windows.Forms.Padding(0);
            this.dt_new_end.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.dt_new_end.MinimumSize = new System.Drawing.Size(0, 29);
            this.dt_new_end.Name = "dt_new_end";
            this.dt_new_end.Size = new System.Drawing.Size(260, 29);
            this.dt_new_end.TabIndex = 3;
            this.dt_new_end.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // lbl_new_end_date
            // 
            this.lbl_new_end_date.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_new_end_date.Enabled = false;
            this.lbl_new_end_date.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_new_end_date.Location = new System.Drawing.Point(20, 115);
            this.lbl_new_end_date.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_new_end_date.Name = "lbl_new_end_date";
            this.lbl_new_end_date.Size = new System.Drawing.Size(260, 25);
            this.lbl_new_end_date.TabIndex = 2;
            this.lbl_new_end_date.Text = "New End Date";
            this.lbl_new_end_date.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_new_end_date.UseMnemonic = false;
            // 
            // Expiring_Project
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(300, 274);
            this.ControlBox = false;
            this.Controls.Add(this.btn_ignore);
            this.Controls.Add(this.lbl_space02);
            this.Controls.Add(this.btn_extend_date);
            this.Controls.Add(this.lbl_space01);
            this.Controls.Add(this.dt_new_end);
            this.Controls.Add(this.lbl_new_end_date);
            this.Controls.Add(this.tb_project_name);
            this.Controls.Add(this.lbl_project_name);
            this.Font = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Movable = false;
            this.Name = "Expiring_Project";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Style = MetroFramework.MetroColorStyle.Red;
            this.Text = "Expiring Project !";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.Load += new System.EventHandler(this.Expiring_Project_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox tb_project_name;
        private MetroFramework.Controls.MetroLabel lbl_project_name;
        private MetroFramework.Controls.MetroButton btn_ignore;
        private MetroFramework.Controls.MetroLabel lbl_space02;
        private MetroFramework.Controls.MetroButton btn_extend_date;
        private MetroFramework.Controls.MetroLabel lbl_space01;
        private MetroFramework.Controls.MetroDateTime dt_new_end;
        private MetroFramework.Controls.MetroLabel lbl_new_end_date;
    }
}
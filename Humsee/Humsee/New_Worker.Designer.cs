﻿namespace Humsee
{
    partial class New_Worker
    {
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.lbl_email = new MetroFramework.Controls.MetroLabel();
            this.lbl_position = new MetroFramework.Controls.MetroLabel();
            this.lbl_last_name = new MetroFramework.Controls.MetroLabel();
            this.lbl_first_name = new MetroFramework.Controls.MetroLabel();
            this.lbl_phone_number = new MetroFramework.Controls.MetroLabel();
            this.lbl_space02 = new MetroFramework.Controls.MetroLabel();
            this.lbl_space01 = new MetroFramework.Controls.MetroLabel();
            this.tb_email = new MetroFramework.Controls.MetroTextBox();
            this.tb_position = new MetroFramework.Controls.MetroTextBox();
            this.tb_last_name = new MetroFramework.Controls.MetroTextBox();
            this.tb_first_name = new MetroFramework.Controls.MetroTextBox();
            this.tb_phone_number = new MetroFramework.Controls.MetroTextBox();
            this.btn_cancel = new MetroFramework.Controls.MetroButton();
            this.btn_create = new MetroFramework.Controls.MetroButton();
            this.SuspendLayout();
            // 
            // lbl_email
            // 
            this.lbl_email.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_email.Enabled = false;
            this.lbl_email.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_email.Location = new System.Drawing.Point(20, 225);
            this.lbl_email.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_email.Name = "lbl_email";
            this.lbl_email.Size = new System.Drawing.Size(260, 25);
            this.lbl_email.TabIndex = 6;
            this.lbl_email.Text = "E-Mail";
            this.lbl_email.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_email.UseMnemonic = false;
            // 
            // lbl_position
            // 
            this.lbl_position.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_position.Enabled = false;
            this.lbl_position.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_position.Location = new System.Drawing.Point(20, 170);
            this.lbl_position.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_position.Name = "lbl_position";
            this.lbl_position.Size = new System.Drawing.Size(260, 25);
            this.lbl_position.TabIndex = 4;
            this.lbl_position.Text = "Position";
            this.lbl_position.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_position.UseMnemonic = false;
            // 
            // lbl_last_name
            // 
            this.lbl_last_name.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_last_name.Enabled = false;
            this.lbl_last_name.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_last_name.Location = new System.Drawing.Point(20, 115);
            this.lbl_last_name.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_last_name.Name = "lbl_last_name";
            this.lbl_last_name.Size = new System.Drawing.Size(260, 25);
            this.lbl_last_name.TabIndex = 2;
            this.lbl_last_name.Text = "Last Name";
            this.lbl_last_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_last_name.UseMnemonic = false;
            // 
            // lbl_first_name
            // 
            this.lbl_first_name.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_first_name.Enabled = false;
            this.lbl_first_name.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_first_name.Location = new System.Drawing.Point(20, 60);
            this.lbl_first_name.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_first_name.Name = "lbl_first_name";
            this.lbl_first_name.Size = new System.Drawing.Size(260, 25);
            this.lbl_first_name.TabIndex = 0;
            this.lbl_first_name.Text = "First Name";
            this.lbl_first_name.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_first_name.UseMnemonic = false;
            // 
            // lbl_phone_number
            // 
            this.lbl_phone_number.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_phone_number.Enabled = false;
            this.lbl_phone_number.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lbl_phone_number.Location = new System.Drawing.Point(20, 280);
            this.lbl_phone_number.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_phone_number.Name = "lbl_phone_number";
            this.lbl_phone_number.Size = new System.Drawing.Size(260, 25);
            this.lbl_phone_number.TabIndex = 8;
            this.lbl_phone_number.Text = "Phone Number";
            this.lbl_phone_number.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lbl_phone_number.UseMnemonic = false;
            // 
            // lbl_space02
            // 
            this.lbl_space02.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_space02.Enabled = false;
            this.lbl_space02.Location = new System.Drawing.Point(20, 380);
            this.lbl_space02.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_space02.Name = "lbl_space02";
            this.lbl_space02.Size = new System.Drawing.Size(260, 5);
            this.lbl_space02.TabIndex = 12;
            this.lbl_space02.UseMnemonic = false;
            // 
            // lbl_space01
            // 
            this.lbl_space01.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl_space01.Enabled = false;
            this.lbl_space01.Location = new System.Drawing.Point(20, 335);
            this.lbl_space01.Margin = new System.Windows.Forms.Padding(0);
            this.lbl_space01.Name = "lbl_space01";
            this.lbl_space01.Size = new System.Drawing.Size(260, 10);
            this.lbl_space01.TabIndex = 10;
            this.lbl_space01.UseMnemonic = false;
            // 
            // tb_email
            // 
            // 
            // 
            // 
            this.tb_email.CustomButton.Font = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.tb_email.CustomButton.Image = null;
            this.tb_email.CustomButton.Location = new System.Drawing.Point(232, 2);
            this.tb_email.CustomButton.Name = "";
            this.tb_email.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tb_email.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_email.CustomButton.TabIndex = 1;
            this.tb_email.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_email.CustomButton.UseSelectable = true;
            this.tb_email.CustomButton.Visible = false;
            this.tb_email.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_email.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_email.Lines = new string[0];
            this.tb_email.Location = new System.Drawing.Point(20, 250);
            this.tb_email.Margin = new System.Windows.Forms.Padding(0);
            this.tb_email.MaxLength = 50;
            this.tb_email.Name = "tb_email";
            this.tb_email.PasswordChar = '\0';
            this.tb_email.PromptText = "example@mail.com";
            this.tb_email.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_email.SelectedText = "";
            this.tb_email.SelectionLength = 0;
            this.tb_email.SelectionStart = 0;
            this.tb_email.ShortcutsEnabled = true;
            this.tb_email.Size = new System.Drawing.Size(260, 30);
            this.tb_email.TabIndex = 7;
            this.tb_email.UseSelectable = true;
            this.tb_email.WaterMark = "example@mail.com";
            this.tb_email.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_email.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tb_position
            // 
            // 
            // 
            // 
            this.tb_position.CustomButton.Image = null;
            this.tb_position.CustomButton.Location = new System.Drawing.Point(232, 2);
            this.tb_position.CustomButton.Name = "";
            this.tb_position.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tb_position.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_position.CustomButton.TabIndex = 1;
            this.tb_position.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_position.CustomButton.UseSelectable = true;
            this.tb_position.CustomButton.Visible = false;
            this.tb_position.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_position.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_position.Lines = new string[0];
            this.tb_position.Location = new System.Drawing.Point(20, 195);
            this.tb_position.Margin = new System.Windows.Forms.Padding(0);
            this.tb_position.MaxLength = 15;
            this.tb_position.Name = "tb_position";
            this.tb_position.PasswordChar = '\0';
            this.tb_position.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_position.SelectedText = "";
            this.tb_position.SelectionLength = 0;
            this.tb_position.SelectionStart = 0;
            this.tb_position.ShortcutsEnabled = true;
            this.tb_position.Size = new System.Drawing.Size(260, 30);
            this.tb_position.TabIndex = 5;
            this.tb_position.UseSelectable = true;
            this.tb_position.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_position.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            // 
            // tb_last_name
            // 
            // 
            // 
            // 
            this.tb_last_name.CustomButton.Image = null;
            this.tb_last_name.CustomButton.Location = new System.Drawing.Point(232, 2);
            this.tb_last_name.CustomButton.Name = "";
            this.tb_last_name.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tb_last_name.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_last_name.CustomButton.TabIndex = 1;
            this.tb_last_name.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_last_name.CustomButton.UseSelectable = true;
            this.tb_last_name.CustomButton.Visible = false;
            this.tb_last_name.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_last_name.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_last_name.Lines = new string[0];
            this.tb_last_name.Location = new System.Drawing.Point(20, 140);
            this.tb_last_name.Margin = new System.Windows.Forms.Padding(0);
            this.tb_last_name.MaxLength = 15;
            this.tb_last_name.Name = "tb_last_name";
            this.tb_last_name.PasswordChar = '\0';
            this.tb_last_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_last_name.SelectedText = "";
            this.tb_last_name.SelectionLength = 0;
            this.tb_last_name.SelectionStart = 0;
            this.tb_last_name.ShortcutsEnabled = true;
            this.tb_last_name.Size = new System.Drawing.Size(260, 30);
            this.tb_last_name.TabIndex = 3;
            this.tb_last_name.UseSelectable = true;
            this.tb_last_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_last_name.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            // 
            // tb_first_name
            // 
            // 
            // 
            // 
            this.tb_first_name.CustomButton.Image = null;
            this.tb_first_name.CustomButton.Location = new System.Drawing.Point(232, 2);
            this.tb_first_name.CustomButton.Name = "";
            this.tb_first_name.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tb_first_name.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_first_name.CustomButton.TabIndex = 1;
            this.tb_first_name.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_first_name.CustomButton.UseSelectable = true;
            this.tb_first_name.CustomButton.Visible = false;
            this.tb_first_name.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_first_name.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_first_name.Lines = new string[0];
            this.tb_first_name.Location = new System.Drawing.Point(20, 85);
            this.tb_first_name.Margin = new System.Windows.Forms.Padding(0);
            this.tb_first_name.MaxLength = 15;
            this.tb_first_name.Name = "tb_first_name";
            this.tb_first_name.PasswordChar = '\0';
            this.tb_first_name.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_first_name.SelectedText = "";
            this.tb_first_name.SelectionLength = 0;
            this.tb_first_name.SelectionStart = 0;
            this.tb_first_name.ShortcutsEnabled = true;
            this.tb_first_name.Size = new System.Drawing.Size(260, 30);
            this.tb_first_name.TabIndex = 1;
            this.tb_first_name.UseSelectable = true;
            this.tb_first_name.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_first_name.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            // 
            // tb_phone_number
            // 
            // 
            // 
            // 
            this.tb_phone_number.CustomButton.Image = null;
            this.tb_phone_number.CustomButton.Location = new System.Drawing.Point(232, 2);
            this.tb_phone_number.CustomButton.Name = "";
            this.tb_phone_number.CustomButton.Size = new System.Drawing.Size(25, 25);
            this.tb_phone_number.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.tb_phone_number.CustomButton.TabIndex = 1;
            this.tb_phone_number.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.tb_phone_number.CustomButton.UseSelectable = true;
            this.tb_phone_number.CustomButton.Visible = false;
            this.tb_phone_number.Dock = System.Windows.Forms.DockStyle.Top;
            this.tb_phone_number.FontSize = MetroFramework.MetroTextBoxSize.Tall;
            this.tb_phone_number.Lines = new string[0];
            this.tb_phone_number.Location = new System.Drawing.Point(20, 305);
            this.tb_phone_number.Margin = new System.Windows.Forms.Padding(0);
            this.tb_phone_number.MaxLength = 11;
            this.tb_phone_number.Name = "tb_phone_number";
            this.tb_phone_number.PasswordChar = '\0';
            this.tb_phone_number.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tb_phone_number.SelectedText = "";
            this.tb_phone_number.SelectionLength = 0;
            this.tb_phone_number.SelectionStart = 0;
            this.tb_phone_number.ShortcutsEnabled = true;
            this.tb_phone_number.Size = new System.Drawing.Size(260, 30);
            this.tb_phone_number.TabIndex = 9;
            this.tb_phone_number.UseSelectable = true;
            this.tb_phone_number.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.tb_phone_number.WaterMarkFont = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackColor = System.Drawing.Color.Crimson;
            this.btn_cancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_cancel.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_cancel.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_cancel.ForeColor = System.Drawing.Color.White;
            this.btn_cancel.Location = new System.Drawing.Point(20, 385);
            this.btn_cancel.Margin = new System.Windows.Forms.Padding(0);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(260, 35);
            this.btn_cancel.TabIndex = 13;
            this.btn_cancel.Text = "Cancel";
            this.btn_cancel.UseCustomBackColor = true;
            this.btn_cancel.UseCustomForeColor = true;
            this.btn_cancel.UseMnemonic = false;
            this.btn_cancel.UseSelectable = true;
            this.btn_cancel.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // btn_create
            // 
            this.btn_create.BackColor = System.Drawing.Color.OliveDrab;
            this.btn_create.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_create.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_create.FontSize = MetroFramework.MetroButtonSize.Medium;
            this.btn_create.ForeColor = System.Drawing.Color.White;
            this.btn_create.Location = new System.Drawing.Point(20, 345);
            this.btn_create.Margin = new System.Windows.Forms.Padding(0);
            this.btn_create.Name = "btn_create";
            this.btn_create.Size = new System.Drawing.Size(260, 35);
            this.btn_create.TabIndex = 11;
            this.btn_create.Text = "Create";
            this.btn_create.UseCustomBackColor = true;
            this.btn_create.UseCustomForeColor = true;
            this.btn_create.UseMnemonic = false;
            this.btn_create.UseSelectable = true;
            this.btn_create.Click += new System.EventHandler(this.Btn_Create_Click);
            // 
            // New_Worker
            // 
            this.AcceptButton = this.btn_create;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.CancelButton = this.btn_cancel;
            this.ClientSize = new System.Drawing.Size(300, 440);
            this.ControlBox = false;
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.lbl_space02);
            this.Controls.Add(this.btn_create);
            this.Controls.Add(this.lbl_space01);
            this.Controls.Add(this.tb_phone_number);
            this.Controls.Add(this.lbl_phone_number);
            this.Controls.Add(this.tb_email);
            this.Controls.Add(this.lbl_email);
            this.Controls.Add(this.tb_position);
            this.Controls.Add(this.lbl_position);
            this.Controls.Add(this.tb_last_name);
            this.Controls.Add(this.lbl_last_name);
            this.Controls.Add(this.tb_first_name);
            this.Controls.Add(this.lbl_first_name);
            this.Font = new System.Drawing.Font("Sitka Banner", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Movable = false;
            this.Name = "New_Worker";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.AeroShadow;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Style = MetroFramework.MetroColorStyle.Green;
            this.Text = "New Worker";
            this.TextAlign = MetroFramework.Forms.MetroFormTextAlign.Center;
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox tb_email;
        private MetroFramework.Controls.MetroTextBox tb_position;
        private MetroFramework.Controls.MetroTextBox tb_last_name;
        private MetroFramework.Controls.MetroTextBox tb_first_name;
        private MetroFramework.Controls.MetroTextBox tb_phone_number;
        private MetroFramework.Controls.MetroButton btn_cancel;
        private MetroFramework.Controls.MetroButton btn_create;
        private MetroFramework.Controls.MetroLabel lbl_email;
        private MetroFramework.Controls.MetroLabel lbl_position;
        private MetroFramework.Controls.MetroLabel lbl_last_name;
        private MetroFramework.Controls.MetroLabel lbl_first_name;
        private MetroFramework.Controls.MetroLabel lbl_phone_number;
        private MetroFramework.Controls.MetroLabel lbl_space02;
        private MetroFramework.Controls.MetroLabel lbl_space01;
    }
}
﻿using System;
using MetroFramework.Forms;

namespace Humsee
{
    public partial class Register : MetroForm
    {
        public Register()
        {
            InitializeComponent();
        }

        private void Btn_Register_Click(object sender, EventArgs e)
        {
            MemberUnit.Register(tb_username.Text, tb_first_name.Text, tb_last_name.Text, tb_password.Text, tb_confirm_password.Text);
        }

        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            Register.ActiveForm.Dispose();
        }
    }
}

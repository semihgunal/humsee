USE [master]
GO
/****** Object:  Database [Humsee]    Script Date: 22.12.2017 18:58:27 ******/
CREATE DATABASE [Humsee]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Humsee', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Humsee.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'Humsee_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\Humsee_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [Humsee] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Humsee].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Humsee] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Humsee] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Humsee] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Humsee] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Humsee] SET ARITHABORT OFF 
GO
ALTER DATABASE [Humsee] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Humsee] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Humsee] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Humsee] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Humsee] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Humsee] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Humsee] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Humsee] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Humsee] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Humsee] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Humsee] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Humsee] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Humsee] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Humsee] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Humsee] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Humsee] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Humsee] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Humsee] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Humsee] SET  MULTI_USER 
GO
ALTER DATABASE [Humsee] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Humsee] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Humsee] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Humsee] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Humsee] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Humsee] SET QUERY_STORE = OFF
GO
USE [Humsee]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [Humsee]
GO
/****** Object:  Table [dbo].[Activity]    Script Date: 22.12.2017 18:58:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Activity](
	[Operation] [varchar](200) NULL,
	[Message] [varchar](200) NULL,
	[Date] [varchar](200) NULL,
	[Project Name] [varchar](200) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Members]    Script Date: 22.12.2017 18:58:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Members](
	[User Name] [varchar](200) NULL,
	[First Name] [varchar](200) NULL,
	[Last Name] [varchar](200) NULL,
	[Password] [varchar](200) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Projects]    Script Date: 22.12.2017 18:58:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Projects](
	[Name] [varchar](200) NULL,
	[Description] [varchar](200) NULL,
	[Language] [varchar](200) NULL,
	[Link] [varchar](200) NULL,
	[Start Date] [varchar](200) NULL,
	[End Date] [varchar](200) NULL,
	[User Name] [varchar](200) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tasks]    Script Date: 22.12.2017 18:58:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tasks](
	[Worker] [varchar](200) NULL,
	[Task] [varchar](200) NULL,
	[Task Status] [varchar](200) NULL,
	[Start Date] [varchar](200) NULL,
	[End Date] [varchar](200) NULL,
	[Project Name] [varchar](200) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Workers]    Script Date: 22.12.2017 18:58:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Workers](
	[First Name] [varchar](200) NULL,
	[Last Name] [varchar](200) NULL,
	[Position] [varchar](200) NULL,
	[Email] [varchar](200) NULL,
	[Phone Number] [varchar](200) NULL,
	[Project Name] [varchar](200) NULL
) ON [PRIMARY]
GO
USE [master]
GO
ALTER DATABASE [Humsee] SET  READ_WRITE 
GO
